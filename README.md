# Mapa Web Interactivo de Obras de la ESSAP


## Migración de base de datos

Se utiliza flyway, migrar con:

    mvn compile flyway:migrate

## Compilación y pruebas

Para compilar el proyecto se debe utilizar:

    mvn clean package

Las pruebas de integración serán ejecutadas automáticamente con un navegador
`Firefox`

Para ejecutar un test especifico utilizar:

    mvn clean test -Pwildfly-remote-arquillian,browser-firefox -Dtest=MedicoMBTest

El mismo ejecutará el archivo `EmployeeCucumberTest` en Firefox.

Para pruebas de integración continua, utilizar el profile `browser-phantomjs`, el
cual no necesita una interfaz gráfica para ser utilizado.

## Datasource

El datasource debe ser similar a:

```xml
<datasource jta="true" jndi-name="java:/mawio" pool-name="MawioDS" enabled="true" use-ccm="true">
    <connection-url>jdbc:postgresql://localhost:5432/mawio?stringtype=unspecified</connection-url>
    <driver-class>org.postgresql.Driver</driver-class>
    <driver>postgresql-9.1-901.jdbc4.jar</driver>
    <security>
        <user-name>mawio</user-name>
        <password>mawioUserPassword</password>
    </security>
    <validation>
        <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLValidConnectionChecker"/>
        <background-validation>true</background-validation>
        <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLExceptionSorter"/>
    </validation>
</datasource>

```
# Agregar dependencias sin maven

Para agregar dependencias sin maven se debe ejecutar:

    mvn deploy:deploy-file -DgroupId=cups4j -DartifactId=cups4j -Dversion=0.6.1 
    agregar al pom

No se debe olvidar commitear los cambios realizados a .m2


# Restauración de Backup
Para su caso particular, tal vez sea necesario ingresar el usuario con -U

```bash
psql -d newmawio -1 -f newmawio_15-06-2016.sql
```

