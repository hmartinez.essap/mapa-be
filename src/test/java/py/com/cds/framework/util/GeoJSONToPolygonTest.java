package py.com.cds.framework.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.geolatte.geom.C2D;
import org.geolatte.geom.Polygon;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import py.com.cds.framework.rest.ObjectMapperProvider;

public class GeoJSONToPolygonTest {

	ObjectMapper om;

	public static final String COORDINATES = "[[[-10.0,-10.0],[10.0,-10.0],[10.0,10.0],[-10.0,10.0],[-10.0,-10.0]]]";

	@Before
	public void init() {

		om = new ObjectMapperProvider().getContext(null);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testNotNull() throws Exception {

		Polygon<C2D> result = om.readValue(example(), Polygon.class);
		assertNotNull(result);

		String asString = om.writeValueAsString(result);
		System.out.println(asString);
		assertTrue(asString.contains(COORDINATES));

	}

	private String example() {

		return "{\"type\": \"Polygon\", \"coordinates\": " + COORDINATES + " }";
	}

}
