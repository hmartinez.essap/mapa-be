package py.com.cds.framework.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import py.com.mawio.be.model.Comentario;
import py.com.mawio.mail.Template;
import py.com.mawio.mail.TemplateCompiler;
import py.com.mawio.security.model.Usuario;

public class TestSimpleTemplate {

	@Test
	public void testName() throws Exception {

		Config c = new Config();
		c.init();

		TemplateCompiler tc = new TemplateCompiler();
		tc.setConfig(c);
		tc.init();

		Map<String, Object> parametros = new HashMap<>();
		Comentario comentario = new Comentario();
		Usuario u = new Usuario();
		
		//comentario.setEmail("pollolocorb@hotmail.com");
		//comentario.setCodigo(RandomStringUtils.randomNumeric(10));
		parametros.put("comentario", comentario);
		parametros.put("usuario", u);

		String templateFinal = tc.buildHtml(Template.EMAIL_MENSAJE_RESPONDIDO, parametros);

		System.out.println(templateFinal);

	}
}
