package py.com.mawio.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import io.swagger.jaxrs.config.BeanConfig;

/**
 * @author Arturo Volpe
 * @since 10/8/2014.
 */
@ApplicationPath("/rest/")
public class RestApplication extends Application {

	public RestApplication() {
		BeanConfig beanConfig = new BeanConfig();

		beanConfig.setTitle("MAWIO API");
		beanConfig.setDescription("Mawio REST API");
		beanConfig.setVersion("1.0");
		
		beanConfig.setSchemes(new String[] { "http" });
		beanConfig.setBasePath("/rest");
		beanConfig.setResourcePackage("py.com");
		beanConfig.setScan(true);
	}
}
