package py.com.mawio.be.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;
import py.com.mawio.obras.model.Obra;
import py.com.mawio.obras.model.Shape;

@Entity
@Data
@Table(name = "contrato")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "ContratoIdGenerator", sequenceName = "contrato_id_sequence", allocationSize = 1, initialValue = 1)
public class Contrato extends BaseEntity {

	private static final long serialVersionUID = -5600506849440437919L;

	public static enum Visible {
		SI, NO
	}

	@Id
	@GeneratedValue(generator = "ContratoIdGenerator")
	private long id;

	@NotNull
	@Temporal(TemporalType.DATE)
	private Date fechaInicio;

	@Temporal(TemporalType.DATE)
	private Date fechaFin;

	@NotNull
	@ManyToOne
	private Obra obra;

	@NotNull
	@ManyToOne
	private Proveedor proveedor;

	@JsonIgnore
	@OneToMany(mappedBy = "contrato")
	private List<Shape> shape;

	private String linkDncp;

	@Enumerated(EnumType.STRING)
	private Visible visible;

	@OneToOne
	@JoinColumn(name = "contrato_dncp_id")
	private ContratoDNCP contratoDNCP;

	@Temporal(TemporalType.DATE)
	private Date fechaFirmaContrato;

	private long montoTotal;

	private long montoDesembolsado;


}