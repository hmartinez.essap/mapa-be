package py.com.mawio.be.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;

@Entity
@Data
@Table(name = "proveedor")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "ProveedorIdGenerator", sequenceName = "proveedor_id_sequence", allocationSize = 1, initialValue = 1)

public class Proveedor extends BaseEntity {

	private static final long serialVersionUID = -2738355755639057456L;

	@Id
	@GeneratedValue(generator = "ProveedorIdGenerator")
	private long id;

	@NotNull
	@Size(max = 100, min = 3)
	private String razonSocial;

	@NotNull
	@Size(max = 100, min = 3)
	private String ruc;

}
