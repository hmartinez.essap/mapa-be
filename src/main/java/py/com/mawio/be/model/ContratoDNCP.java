package py.com.mawio.be.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;

@Entity
@Data
@Table(name = "contrato_dncp")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "ContratoDNCPIdGenerator", sequenceName = "contrato_dncp_id_sequence", allocationSize = 1, initialValue = 1)
public class ContratoDNCP extends BaseEntity {

	private static final long serialVersionUID = -2158058389904908948L;

	@Id
	@GeneratedValue(generator = "ContratoDNCPIdGenerator")
	private long id;

	@Size(max = 100, min = 3)
	private String ruc;

	@Size(max = 100, min = 3)
	private String razonSocial;

	@NotNull
	@Size(max = 100, min = 3)
	@Column(name = "cod_contratacion")
	private String codContratacion;

	@Min(0)
	@Column(name = "nro_contratacion")
	private long nroContratacion;

	private Long montoAdjudicado;

	@Size(max = 100, min = 3)
	private String estado;

	@Size(max = 100, min = 3)
	private String moneda;

	@Size(max = 300, min = 3)
	private String linkDncp;

	@Size(max = 100, min = 3)
	private String idLlamado;

	@Temporal(TemporalType.DATE)
	private Date fechaContrato;

}
