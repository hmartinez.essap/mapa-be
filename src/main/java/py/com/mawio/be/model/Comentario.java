package py.com.mawio.be.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.geolatte.geom.C2D;
import org.geolatte.geom.Point;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;
import py.com.mawio.obras.model.Shape;
import py.com.mawio.security.model.Usuario;

@Entity
@Data
@Table(name = "comentario")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "ComentarioIdGenerator", sequenceName = "comentario_id_sequence", allocationSize = 1, initialValue = 1)

public class Comentario extends BaseEntity {

	private static final long serialVersionUID = 8404915514701342657L;

	@Id
	@GeneratedValue(generator = "ComentarioIdGenerator")
	private long id;

	public static enum Estado {
		RESPONDIDO, SINRESPONDER
	}

	@NotNull
	private String comentario;

	private Double longitud;
	private Double latitud;

	@NotNull
	@Size(max = 100, min = 3)
	private String email;

	@Column(name = "telefono_contacto")
	@Size(max = 100, min = 3)
	private String telefonoContacto;

	@NotNull
	@Temporal(TemporalType.DATE)
	private Date fechaEmision;

	@Temporal(TemporalType.DATE)
	private Date fechaRespuesta;

	@Size(max = 100, min = 3)
	private String respuesta;

	@Enumerated(EnumType.STRING)
	private Estado estado;

	@ManyToOne
	@JoinColumn(name = "usuario_respuesta")
	private Usuario usuarioRespuesta;

	@NotNull
	@ManyToOne
	private Shape shape;

	@NotNull
	private String codigo;

}
