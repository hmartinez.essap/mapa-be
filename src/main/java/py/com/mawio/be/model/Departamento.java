package py.com.mawio.be.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;

@Entity
@Data
@EqualsAndHashCode(callSuper = true, of = "id")
@Table(name = "departamento")
@SequenceGenerator(name = "DepartamentoIdGenerator", sequenceName = "departamento_id_sequence")
public class Departamento extends BaseEntity {

	private static final long serialVersionUID = 5302343317916184366L;

	@Id
	@GeneratedValue(generator = "DepartamentoIdGenerator")
	private long id;

	@NotNull
	@Size(max = 100, min = 3)
	private String nombre;

	@NotNull
	@Size(max = 100, min = 3)
	@Column(name = "codigo", unique = true)
	private String codigo;

}
