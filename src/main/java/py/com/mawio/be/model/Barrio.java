package py.com.mawio.be.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;

@Entity
@Data
@EqualsAndHashCode(callSuper = true, of = "id")
@Table(name = "barrio")
@SequenceGenerator(name = "BarrioIdGenerators", sequenceName = "barrio_id_seq")
public class Barrio extends BaseEntity {

	private static final long serialVersionUID = -308552201665482178L;

	@Id
	@GeneratedValue(generator = "BarrioIdGenerators")
	@Column(columnDefinition = "serial")
	private long id;

	@NotNull
	@Size(max = 100, min = 3)
	@Column(name = "codigo", unique = true)
	private String codigo;

	@NotNull
	@Size(max = 100, min = 3)
	@Column(name = "dpto")
	private String codigoDepartamento;

	@NotNull
	@Size(max = 100, min = 3)
	@Column(name = "dpto_desc")
	private String departamentoDescripcion;

	@NotNull
	@Size(max = 100, min = 3)
	@Column(name = "dist_desc")
	private String distritoDescripcion;
	
	@NotNull
	@Size(max = 100, min = 3)
	@Column(name = "distrito")
	private String distritoCodigo;
	
	@NotNull
	@Size(max = 100, min = 3)
	@Column(name = "barlo_desc")
	private String barrioDescripcion;
	
	@NotNull
	@Size(max = 100, min = 3)
	@Column(name = "bar_loc")
	private String barrioCod;

}