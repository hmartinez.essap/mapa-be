package py.com.mawio.mail;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import py.com.cds.framework.util.Config;

@ApplicationScoped
public class SessionProvider {

	@Produces
	private Session session;

	@Inject
	private Config config;

	@PostConstruct
	void init() {
		this.session = buildSession();
	}

	protected Session buildSession() {
		Properties props = new Properties();
		props.put("mail.smtp.host", config.getString("mail.smtp.host"));
		props.put("mail.smtp.port", config.getInteger("mail.smtp.port", 25));
		// props.put("mail.smtp.ssl.enable", true);
		props.put("mail.smtp.starttls.enable", true);

		String user = config.getString("mail.user");
		String pass = config.getString("mail.pass");
		props.setProperty("mail.smtp.user", user);
		props.setProperty("mail.smtp.password", pass);
		props.setProperty("mail.smtp.auth", "true");

		boolean isDebug = config.getBoolean("mail.debug", false);

		Authenticator auth = new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, pass);
			}
		};

		Session s = Session.getInstance(props, auth);
		s.setDebug(isDebug);
		return s;
	}

}
