package py.com.mawio.mail;

public enum Template {

	EMAIL_MENSAJE_ENVIADO("template1", "mail.sender", "mail.title.envio"),
	EMAIL_MENSAJE_RESPONDIDO("template2", "mail.sender", "mail.title.respuesta");

	private String filename;
	private String titleKey;
	private String senderKey;

	Template(String filename, String titleKey, String senderKey) {
		this.filename = filename;
		this.titleKey = titleKey;
		this.senderKey = senderKey;
	}

	/**
	 * Returns the file name that this template uses.
	 */
	public String getFilename() {
		return this.filename;
	}

	public String getTitleKey() {
		return titleKey;
	}

	public String getSenderKey() {
		return senderKey;
	}
}
