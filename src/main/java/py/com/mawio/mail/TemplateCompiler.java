package py.com.mawio.mail;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.log.NullLogChute;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import lombok.Setter;
import py.com.cds.framework.util.Config;

/**
 * Compiles a Template, and returns the HTML.
 * 
 * @author Arturo Volpe
 *
 */
@Stateless
public class TemplateCompiler {

	@Inject
	@Setter
	private Config config;

	private Map<String, String> defaultParmas = new HashMap<>();

	@PostConstruct
	public void init() {

		Velocity.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, NullLogChute.class.getName());
		Velocity.setProperty(RuntimeConstants.INPUT_ENCODING, "UTF-8");
		Velocity.setProperty(RuntimeConstants.OUTPUT_ENCODING, "UTF-8");

		Velocity.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		Velocity.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());

		Velocity.init();

	}

	/**
	 * Compiles a template and returns it html form.
	 * 
	 * @param template
	 * @param params
	 * @return
	 */
	public String buildHtml(py.com.mawio.mail.Template template, Map<String, Object> params) {

		return buildHtml(template.getFilename(), params);
	}

	/**
	 * Compiles a template and returns it html form.
	 * 
	 * @param template
	 * @param params
	 * @return
	 */
	public String buildHtml(String name, Map<String, Object> params) {

		return render(getPathForTemplate(name), params);
	}

	private String getPathForTemplate(String filename) {
		return String.format("velocity/%s.html", filename);
	}

	private org.apache.velocity.Template getTemplate(String source) {

		return Velocity.getTemplate(source);
	}

	private String render(String name, Map<String, Object> parametros) {
		VelocityContext context = createContext(parametros);
		StringWriter out = new StringWriter();
		org.apache.velocity.Template template = getTemplate(name);
		template.merge(context, out);
		return out.toString();
	}

	private VelocityContext createContext(Map<String, Object> parameters) {
		VelocityContext context = new VelocityContext();
		for (Entry<String, String> def : defaultParmas.entrySet())
			context.put(def.getKey(), def.getValue());
		for (Entry<String, Object> def : parameters.entrySet())
			context.put(def.getKey(), def.getValue());
		return context;
	}
}
