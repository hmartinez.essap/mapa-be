package py.com.mawio.mail;

import java.util.Date;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.Logger;

import py.com.cds.framework.util.Config;

/**
 * Stateless EJB that sends mails.
 * 
 * @author Arturo Volpe
 *
 */
@Stateless
public class MailSender {

	@Inject
	private TemplateCompiler compiler;

	@Inject
	private Config config;

	@Inject
	private Session session;

	@Inject
	private Logger logger;

	/**
	 * Sends a mail to a user email.
	 * 
	 * @param email
	 *            the not <code>null</code> email to send the message.
	 * @param template
	 *            the not <code>null</code> template to use.
	 * @param params
	 *            the params of the template.
	 */
	public void sendMail(String email, Template template, Map<String, Object> params) {

		Validate.notBlank(email, "Invalid email");
		Validate.notNull(template, "Invalid template");
		Validate.notNull(params, "Invalid params");

		logger.info("Sending mail to {}", email);
		sendHTMLMail(email,
				config.getString(template.getSenderKey()),
				config.getString(template.getTitleKey()),
				compiler.buildHtml(template, params));
	}

	private void sendHTMLMail(String email, String subject, String sender, String html) {

		MimeMessage message = new MimeMessage(session);
		try {
			System.out.println(sender);
			message.setFrom(new InternetAddress(sender));
			InternetAddress[] address = { new InternetAddress(email) };
			message.setRecipients(Message.RecipientType.TO, address);
			message.setSubject(subject, "UTF-8");
			message.setSentDate(new Date());

			message.setContent(buildMultiPart(html));

			Transport.send(message);
		} catch (MessagingException ex) {
			throw new RuntimeException("Can't send mail", ex);
		}
	}

	private Multipart buildMultiPart(String html) throws MessagingException {
		Multipart multipart = new MimeMultipart("alternative");

		MimeBodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(html, "text/html");

		multipart.addBodyPart(htmlPart);
		return multipart;
	}

}
