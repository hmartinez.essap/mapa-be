/**
 * This package holds the logic to send mails to users.
 * 
 * @author Arturo Volpe
 *
 */
package py.com.mawio.mail;