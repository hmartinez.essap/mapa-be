package py.com.mawio.obras.logic;

import java.util.List;

import javax.ejb.Stateless;

import lombok.AllArgsConstructor;
import lombok.Data;
import py.com.cds.framework.ejb.BaseLogic;
import py.com.mawio.be.model.Barrio;
import py.com.mawio.be.model.Departamento;

@Stateless
public class DepartamentoLogic extends BaseLogic<Departamento> {

	//@formatter:off
	public static final String QUERY_DISTRITOS = ""
			+ "	SELECT"
			+ "		new " + DistritoDTO.class.getName()
			+ "		("
			+ "			b.distritoCodigo,"
			+ "			b.distritoDescripcion"
			+ "		)"
			+ "	FROM"
			+ "		Barrio b"
			+ "	WHERE"
			+ "		b.codigoDepartamento = :depto"
			+ "	GROUP BY "
			+ "		b.distritoCodigo,"
			+ "		b.distritoDescripcion"
			+ "	ORDER BY"
			+ "		b.distritoDescripcion ASC";
	//@formatter:on

	public List<Barrio> getAllBarrios(String codDpto,String codDistrito) {
		return getStreamAll(Barrio.class).where(b -> b.getCodigoDepartamento() == codDpto && b.getDistritoCodigo()==codDistrito).toList();
	}

	public List<DistritoDTO> getAllDistritos(String cod) {
		return getEm().createQuery(QUERY_DISTRITOS, DistritoDTO.class).setParameter("depto", cod).getResultList();
	}

	@AllArgsConstructor
	@Data
	public static class DistritoDTO {
		String codigo;
		String descripcion;
	}
}