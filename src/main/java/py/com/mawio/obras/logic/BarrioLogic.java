package py.com.mawio.obras.logic;

import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.lang3.StringUtils;
import org.jinq.jpa.JPQL;

import py.com.cds.framework.ejb.BaseLogic;
import py.com.mawio.be.model.Barrio;

@Stateless
public class BarrioLogic extends BaseLogic<Barrio> {

	public List<Barrio> getFiltered(String globalSearch) {

		if (StringUtils.isEmpty(globalSearch)) {
			throw new IllegalArgumentException("No se puede buscar sin filtro");
		}

		String like = "%" + globalSearch.toLowerCase() + "%";

		return getStreamAll().where(depto -> JPQL.like(depto.getDistritoDescripcion().toLowerCase(), like)).limit(5)
				.toList();
	}
}
