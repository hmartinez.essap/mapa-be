package py.com.mawio.obras.logic;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lombok.Data;
import py.com.cds.framework.ejb.BaseLogic;
import py.com.cds.framework.pictshare.PictShareHelper;
import py.com.mawio.be.model.Contrato;
import py.com.mawio.obras.model.EstadoShape;
import py.com.mawio.obras.model.Foto;
import py.com.mawio.obras.model.Obra;
import py.com.mawio.obras.model.Shape;
import py.com.mawio.obras.model.TipoShape;

@Stateless
public class ShapeLogic extends BaseLogic<Shape> {
	@Inject
	PictShareHelper ps;

	// @formatter:off
	public static final String SUBQUERY_GET_SHAPES = ""
			+ "\n  SELECT  "
			+ "\n              DISTINCT(s.id) AS id"
			+ "\n     FROM"
			+ "\n             shape s"
			+ "\n      JOIN obra o ON o.id = s.obra_id"
			+ "\n  LEFT JOIN barrio b "
			+ "\n              ON 1 = :conFiltroBarrio AND ST_Intersects(b.geom, CAST(CAST(s.geometry AS geography) AS geometry))"
			+ "\n      LEFT JOIN contrato c ON o.id = c.obra_id"
			+ "\n WHERE 1=1"
			+ "\n      WHERE_ADICIONALES ";

	public static final String QUERY_GET_SHAPES = ""
			+ "\n     SELECT  "
			+ "\n              CAST(s.id AS INTEGER),"
			+ "\n           s.descripcion,"
			+ "\n              ST_asGeoJson(s.geometry),"
			+ "\n              CAST(s.tipo_id AS INTEGER),"
			+ "\n              CAST(s.obra_id AS INTEGER),"
			+ "\n              CAST(s.estado_id AS INTEGER),"
			+ "\n              CAST(s.contrato_id AS INTEGER)"
			+ "\n   FROM"
			+ "\n              ( "
			+ SUBQUERY_GET_SHAPES
			+ ") r"
			+ "\n      JOIN shape s ON s.id = r.id ";

	public static final String QUERY_GET_SHAPES_ONLY_OBRAS =
			QUERY_GET_SHAPES
			+ "\n ORDER";

	public static final String QUERY_GET_SHAPES_OF_OBRA =
			QUERY_GET_SHAPES
			+ "    AND s.obra_id = :obraId"
			+ "    AND s.contrato_id IS NOT NULL"
			+ "\n ORDER";

	public static final String QUERY_SET_GEOMETRY =
			"UPDATE shape SET geometry = ST_GeomFromGeoJSON(:shape) WHERE id = :id";
	
	public static final String QUERY_SET_TIPO_GEOM =""
			+ "UPDATE shape SET tipo_geom = 'A_AREA' WHERE id = :id AND ST_GeometryType(shape.geometry) = 'ST_Polygon';"
			+ "UPDATE shape SET tipo_geom = 'B_Longitud' WHERE id = :id AND ST_GeometryType(shape.geometry) = 'ST_LineString';"
	 		+ "UPDATE shape SET tipo_geom = 'C_Punto' WHERE id = :id AND ST_GeometryType(shape.geometry) = 'ST_Point'";
	
	public static final String QUERY_SET_PESO = ""
			+ "UPDATE shape SET peso = ST_Area(shape.geometry) WHERE id = :id AND shape.tipo_geom = 'A_AREA';"
			+ "UPDATE shape SET peso = ST_Length(shape.geometry) WHERE id = :id AND shape.tipo_geom = 'B_Longitud';"
			+ "UPDATE shape SET peso = 99999999 WHERE id = :id AND shape.tipo_geom = 'C_Punto';";
	
	public static final String QUERY_GET_SHAPES_COMPLETO = ""
			+ "\n  SELECT  "
			+ "\n       o.area,"
			+ "\n       o.descripcion,"
			+ "\n       o.fecha_inicio,"
			+ "\n       o.fecha_fin,"
			+ "\n       o.tipo"
			+ "\n   FROM"
			+ "\n       ( " + SUBQUERY_GET_SHAPES + ") r"
			+ "\n   JOIN shape s ON s.id = r.id "
			+ "\n   JOIN obra o ON s.obra_id = o.id";
	
	public static final String QUERY_GET_SHAPES_ONLY_OBRAS_COMPLETO = QUERY_GET_SHAPES_COMPLETO
	        + "\n AND s.contrato_id IS NULL "
	        + "\n ORDER";
	// @formatter:on

	public Foto addImage(Long id, InputStream image) {

		Shape s = findById(id);
		String hashName = ps.uploadAndGetName(image, "png");
		Foto f = new Foto();
		f.setShape(s);
		f.setHash(hashName);
		getEm().persist(f);
		return f;
	}

	public List<Foto> getAllImages(Long id) {

		return getStreamAll(Foto.class).where(f -> f.getShape().getId() == id).toList();
	}

	public ShapeDTO map(Object[] origen) {

		Long id = ((Integer) origen[0]).longValue();
		String descripcion = (String) origen[1];
		String geometry = (String) origen[2];
		Long tipoId = ((Integer) origen[3]).longValue();
		Long obraId = ((Integer) origen[4]).longValue();
		Long estadoId = ((Integer) origen[5]).longValue();

		return new ShapeDTO(id, descripcion, geometry, tipoId, obraId, estadoId);
	}

	public Obra mapObra(Object[] origen) {

		String area = (String) origen[0];
		String descripcion = (String) origen[1];
		Date fechaInicio = (Date) origen[2];
		Date fechaFin = (Date) origen[3];
		String tipo = (String) origen[4];
		Obra obra = new Obra();
		obra.setArea(area);
		obra.setDescripcion(descripcion);
		obra.setFechaFin(fechaFin);
		obra.setFechaInicio(fechaInicio);
		obra.setTipo(tipo);
		return obra;
	}

	@SuppressWarnings("unchecked")
	public List<ShapeDTO> getFiltered(String dpto, String distrito, String barloc, String fechaInicioObra,
			String fechaFinObra, Long montoInferior, Long montoSuperior, Long proveedorId, String fechaInicioContrato,
			String fechaFinContrato, Integer offset, Integer limit, String order) {

		if (limit < 0)
			limit = null;

		Query query = buildQuery(dpto, distrito, barloc, fechaInicioObra, fechaFinObra, montoInferior, montoSuperior,
				proveedorId, fechaInicioContrato, fechaFinContrato, offset, limit, order, QUERY_GET_SHAPES_ONLY_OBRAS);

		return map(query.getResultList());
	}

	@SuppressWarnings("unchecked")
	public List<ShapeDTO> getFilteredByObra(long obraId, String dpto, String distrito, String barloc,
			String fechaInicioObra, String fechaFinObra, Long montoInferior, Long montoSuperior, Long proveedorId,
			String fechaInicioContrato, String fechaFinContrato, Integer offset, Integer limit, String order) {

		Query query = buildQuery(dpto, distrito, barloc, fechaInicioObra, fechaFinObra, montoInferior, montoSuperior,
				proveedorId, fechaInicioContrato, fechaFinContrato, offset, limit, order, QUERY_GET_SHAPES_OF_OBRA);

		query.setParameter("obraId", obraId);
		return map(query.getResultList());
	}

	private Query buildQuery(String dpto, String distrito, String barloc, String fechaInicioObra, String fechaFinObra,
			Long montoInferior, Long montoSuperior, Long proveedorId, String fechaInicioContrato,
			String fechaFinContrato, Integer offset, Integer limit, String order, String queryString) {

		String whereAdicionales = "";
		if (!StringUtils.isEmpty(dpto)) {
			whereAdicionales += " AND b.dpto = :dpto";
		}
		if (!StringUtils.isEmpty(distrito)) {
			whereAdicionales += " AND b.distrito = :distrito";
		}
		if (!StringUtils.isEmpty(barloc)) {
			whereAdicionales += " AND b.bar_loc = :barrio";
		}
		if (!StringUtils.isEmpty(fechaInicioObra)) {
			whereAdicionales += " AND o.fecha_inicio < :fechaInicioObra";
		}
		if (!StringUtils.isEmpty(fechaFinObra)) {
			whereAdicionales += " AND o.fecha_fin > :fechaFinObra";
		}
		if (!(montoInferior == null)) {
			whereAdicionales += " AND c.monto_total > :montoInferior";
		}
		if (!(montoSuperior == null)) {
			whereAdicionales += " AND c.monto_total < :montoSuperior";
		}
		if (!(proveedorId == null)) {
			whereAdicionales += " AND c.proveedor_id = :proveedorId";
		}
		if (!StringUtils.isEmpty(fechaInicioContrato)) {
			whereAdicionales += " AND c.fecha_inicio < :fechaInicioContrato";
		}
		if (!StringUtils.isEmpty(fechaFinContrato)) {
			whereAdicionales += " AND c.fecha_fin > :fechaFinContrato";
		}

		if (!StringUtils.isEmpty(order)) {
			order = "tipo_geom ASC, peso DESC";
			queryString = queryString.replace("ORDER", " ORDER BY " + order);
		} else {
			queryString = queryString.replace("ORDER", "");
		}

		Query query = getEm().createNativeQuery(queryString.replace("WHERE_ADICIONALES", whereAdicionales));

		if (offset != null)
			query.setFirstResult(offset);
		if (limit != null)
			query.setMaxResults(limit);

		addIfNotNull("dpto", dpto, query);
		addIfNotNull("distrito", distrito, query);
		addIfNotNull("barrio", barloc, query);
		addIfNotNull("fechaInicioObra", fechaInicioObra, query);
		addIfNotNull("fechaFinObra", fechaFinObra, query);
		addIfNotNull("fechaInicioContrato", fechaInicioContrato, query);
		addIfNotNull("fechaFinContrato", fechaFinContrato, query);
		addIfNotNull("montoInferior", montoInferior, query);
		addIfNotNull("montoSuperior", montoSuperior, query);
		addIfNotNull("proveedorId", proveedorId, query);

		if (!StringUtils.isEmpty(dpto) || !StringUtils.isEmpty(barloc) || !StringUtils.isEmpty(distrito)) {
			query.setParameter("conFiltroBarrio", 1);
		} else {
			query.setParameter("conFiltroBarrio", 0);
		}

		return query;
	}

	private List<ShapeDTO> map(List<Object[]> result) {

		List<ShapeDTO> resultList = new ArrayList<>();
		for (Object[] o : result) {
			resultList.add(map(o));
		}

		return resultList;
	}

	private void addIfNotNull(String name, Object value, Query query) {

		if (value == null) {
			return;
		}
		if (value instanceof String) {
			String valueS = (String) value;
			if (StringUtils.isEmpty(valueS)) {
				return;
			}
			query.setParameter(name, valueS);
			return;
		} else {
			query.setParameter(name, value);
		}
	}

	public ShapeInfoDTO getInfo(Long id) {

		Shape s = findById(id);
		ShapeInfoDTO toRet = new ShapeInfoDTO();
		toRet.setId(id);
		toRet.setFotos(getAllImages(id));
		toRet.setObra(s.getObra());
		toRet.setContrato(s.getContrato());
		toRet.setDesc(s.getDescripcion());
		toRet.setEstado(s.getEstado());
		toRet.setTipo(s.getTipo());
		return toRet;
	}

	public Shape updateShape(Shape shape, ObjectNode geometry) {

		shape = getEm().merge(shape);

		ObjectNode on = geometry;
		if (on.has("geometry")) {
			on = (ObjectNode) on.get("geometry");
		}

		int cantidadActualizados = getEm().createNativeQuery(QUERY_SET_GEOMETRY).setParameter("shape", on.toString())
				.setParameter("id", shape.getId()).executeUpdate();

		int cantidadActualizadosTipoGeom = getEm().createNativeQuery(QUERY_SET_TIPO_GEOM)
				.setParameter("id", shape.getId()).executeUpdate();
		
		int cantidadActualizadosPeso = getEm().createNativeQuery(QUERY_SET_PESO)
				.setParameter("id", shape.getId()).executeUpdate();
		
		if (cantidadActualizados != 1) {
			System.out.println("Algo raro paso, no se cargo bien el shape");
		}
		if (cantidadActualizadosTipoGeom != 1) {
			System.out.println("Algo raro paso, no se cargo bien el tipo_geom del shape");
		}
		if (cantidadActualizadosPeso != 1) {
			System.out.println("Algo raro paso, no se cargo bien el peso del shape");
		}

		return shape;
	}

	@SuppressWarnings("unchecked")
	public List<Obra> getFilteredCompleto(String dpto, String distrito, String barloc, String fechaInicioObra,
			String fechaFinObra, Long montoInferior, Long montoSuperior, Long proveedorId, String fechaInicioContrato,
			String fechaFinContrato, Integer offset, Integer limit, String order) {

		Query query = buildQuery(dpto, distrito, barloc, fechaInicioObra, fechaFinObra, montoInferior, montoSuperior,
				proveedorId, fechaInicioContrato, fechaFinContrato, offset, limit, order,
				QUERY_GET_SHAPES_ONLY_OBRAS_COMPLETO);

		return mapObras(query.getResultList());
	}

	private List<Obra> mapObras(List<Object[]> result) {

		List<Obra> resultList = new ArrayList<>();
		for (Object[] o : result) {
			resultList.add(mapObra(o));
		}

		return resultList;
	}

	@Data
	public static class ShapeDTO {

		Long id;
		String descripcion;
		JsonNode geometry;
		Long tipo;
		Long obraId;
		Long estadoId;

		public ShapeDTO(Long id, String description, String geometry, Long tipoId, Long obraId, Long estadoId) {
			this.id = id;
			this.descripcion = description;
			this.tipo = tipoId;
			this.obraId = obraId;
			this.estadoId = estadoId;

			if (StringUtils.isBlank(geometry)) {
				return;
			}

			try {
				this.geometry = new ObjectMapper().readTree(geometry);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Data
	public static class ShapeInfoDTO {

		Long id;
		List<Foto> fotos;
		Obra obra;
		Contrato contrato;
		EstadoShape estado;
		TipoShape tipo;
		String desc;

	}

}
