package py.com.mawio.obras.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.torpedoquery.jpa.OnGoingLogicalCondition;
import org.torpedoquery.jpa.Torpedo;

import com.fasterxml.jackson.databind.node.ObjectNode;

import lombok.AllArgsConstructor;
import lombok.Data;
import py.com.cds.framework.ejb.BaseLogic;
import py.com.cds.framework.util.DataWithCount;
import py.com.cds.framework.util.QueryHelper;
import py.com.cds.framework.util.SortInfo;
import py.com.cds.framework.util.Utils;
import py.com.cds.framework.util.QueryHelper.QueryCustomizer;
import py.com.mawio.be.model.Contrato;
import py.com.mawio.obras.logic.ShapeLogic.ShapeDTO;
import py.com.mawio.obras.model.EstadoShape;
import py.com.mawio.obras.model.Obra;
import py.com.mawio.obras.model.Shape;
import py.com.mawio.obras.model.TipoShape;

@Stateless
public class ObraLogic extends BaseLogic<Obra> {

	// @formatter:off
	public static final String QUERY_GET_SHAPES = ""
			+ "	SELECT	"
			+ "	new " + ShapeDTO.class.getName()
			+ "	("
			+ "		s.id,"
			+ "		s.descripcion,"
			+ "		toGeoJson(s.geometry),"
			+ "		s.tipo.id,"
			+ "		s.obra.id,"
			+ "		s.estado.id"
			+ "	)"
			+ "	FROM"
			+ "		Shape s"
			+ "	WHERE"
			+ "		s.obra.id = :obraId";
	// @formatter:on

	@Inject
	private ShapeLogic shapeLogic;

	@Inject
	private ContratoLogic contratoLogic;

	public Shape addShape(Obra o, String descripcion, TipoShape tipo, EstadoShape estado, ObjectNode geometry) {
		Shape shape = new Shape();

		shape.setObra(o);
		shape.setDescripcion(descripcion);
		shape.setTipo(tipo);
		shape.setEstado(estado);

		return shapeLogic.updateShape(shape, geometry);
	}

	public DataWithCount<Contrato> getContratos(Long obraId, Integer start, Integer length, List<String> orders,
			List<String> values, List<String> columns, String globalSearch) {

		List<SortInfo> sortInfo = Utils.deserializeSort(orders);
		Map<String, String> params = Utils.joinAndCheckLists(columns, values, Contrato.class);

		Customizer c = new Customizer(findById(obraId));

		return new DataWithCount<Contrato>(
				new QueryHelper().get(start, length, sortInfo, params, Contrato.class, globalSearch, c).list(getEm()),
				new QueryHelper().getCount(params, Contrato.class, c).get(getEm()),
				new QueryHelper().getCount(Collections.emptyMap(), Contrato.class, c).get(getEm()), 0);
	}

	public List<Contrato> getContratos(Long id) {

		return getStreamAll(Contrato.class).where(c -> c.getObra().getId() == id).toList();
	}

	public Contrato addContrato(Long obraId, Contrato contrato) {

		Obra o = findById(obraId);
		contrato.setObra(o);
		return contratoLogic.add(contrato);
	}

	public List<ShapeDTO> getAllShapes(Long id) {
		return getEm().createQuery(QUERY_GET_SHAPES, ShapeDTO.class).setParameter("obraId", id).getResultList();
	}

	public List<Shape> updateShapes(Long id, List<ShapeWithNode> datos) {
		Obra o = findById(id);
		List<Shape> toRet = new ArrayList<>();
		for (ShapeWithNode shapeWithNode : datos) {
			Shape s = shapeWithNode.getShape();
			s.setObra(o);
			ObjectNode nuevaForma = shapeWithNode.getNode();
			Shape agregado = null;

			if (s.isNew()) {
				agregado = addShape(o, s.getDescripcion(), s.getTipo(), s.getEstado(), nuevaForma);
			} else {
				Shape actualizarShape = shapeLogic.findById(s.getId());
				actualizarShape.setDescripcion(s.getDescripcion());
				actualizarShape.setObra(s.getObra());
				actualizarShape.setTipo(s.getTipo());
				actualizarShape.setEstado(s.getEstado());
				agregado = updateShape(actualizarShape, nuevaForma);
			}
			toRet.add(agregado);
		}

		List<Shape> aBorrar = obtenerShapesABorrar(id, toRet);
		for (Shape ab : aBorrar) {
			shapeLogic.remove(ab);
		}

		return toRet;
	}

	private Shape updateShape(Shape shape, ObjectNode nuevaForma) {
		Shape s = shapeLogic.updateShape(shape, nuevaForma);
		return s;
	}

	private List<Shape> obtenerShapesABorrar(Long id, Collection<Shape> nuevos) {

		List<Shape> borrar = new ArrayList<>();
		List<Shape> shapesViejos = getStreamAll(Shape.class).where(sh -> sh.getObra().getId() == id).toList();
		for (Shape sv : shapesViejos) {
			if (!nuevos.contains(sv)) {
				borrar.add(sv);
			}
		}
		return borrar;
	}

	@Data
	@AllArgsConstructor
	public static class ShapeWithNode {
		Shape shape;
		ObjectNode node;
	}

	private class Customizer extends QueryCustomizer<Contrato> {

		private Obra obra;

		public Customizer(Obra obra) {
			this.obra = obra;
		}

		@Override
		public OnGoingLogicalCondition firstWhere(Contrato from) {

			return Torpedo.where(from.getObra()).eq(this.obra);
		}
	}

}
