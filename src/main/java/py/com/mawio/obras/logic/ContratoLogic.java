package py.com.mawio.obras.logic;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lombok.Data;
import py.com.cds.framework.ejb.BaseLogic;
import py.com.cds.framework.util.Config;
import py.com.mawio.be.model.Contrato;
import py.com.mawio.be.model.ContratoDNCP;
import py.com.mawio.obras.logic.ObraLogic.ShapeWithNode;
import py.com.mawio.obras.logic.ShapeLogic.ShapeDTO;
import py.com.mawio.obras.model.EstadoShape;
import py.com.mawio.obras.model.Obra;
import py.com.mawio.obras.model.Shape;
import py.com.mawio.obras.model.TipoShape;

@Stateless
public class ContratoLogic extends BaseLogic<Contrato> {

	// @formatter:off
	public static final String QUERY_GET_SHAPES_CONTRATOS = ""
			+ "	SELECT	"
			+ "	new " + ShapeDTO.class.getName()
			+ "	("
			+ "		s.id,"
			+ "		s.descripcion,"
			+ "		toGeoJson(s.geometry),"
			+ "		s.tipo.id,"
			+ "		s.obra.id,"
			+ "		s.estado.id"
			+ "	)"
			+ "	FROM"
			+ "		Shape s"
			+ "	WHERE"
			+ "		s.contrato.id = :contratoId";
	// @formatter:on

	@Inject
	ShapeLogic shapeLogic;

	@Inject
	ObraLogic obraLogic;

	@Inject
	ProveedorLogic proveedorLogic;

	@Inject
	ObjectMapper om;

	@Inject
	Config config;

	public Shape addShape(Obra o, Contrato c, String descripcion, TipoShape tipo, EstadoShape estado,
			ObjectNode geometry) {

		Shape shape = new Shape();
		shape.setObra(o);
		shape.setContrato(c);
		shape.setDescripcion(descripcion);
		shape.setTipo(tipo);
		shape.setEstado(estado);

		return shapeLogic.updateShape(shape, geometry);

	}

	public Contrato add(Contrato c) {

		setContratoDNCP(c);
		c.setProveedor(proveedorLogic.add(c.getProveedor()));
		return super.add(c);
	}

	private void setContratoDNCP(Contrato c) {
		if (c.getContratoDNCP().isNew()) {
			ContratoDNCP nuevo = getEm().merge(c.getContratoDNCP());
			c.setContratoDNCP(nuevo);
		}
	}

	@Override
	public Contrato update(Contrato entity) {
		setContratoDNCP(entity);
		entity.setProveedor(proveedorLogic.add(entity.getProveedor()));
		return super.update(entity);
	}

	public List<Shape> updateShapes(Long idContrato, Long idObra, List<ShapeWithNode> datos) {
		Obra o = obraLogic.findById(idObra);
		Contrato c = findById(idContrato);
		List<Shape> toRet = new ArrayList<>();
		for (ShapeWithNode shapeWithNode : datos) {

			Shape s = shapeWithNode.getShape();
			s.setContrato(c);
			s.setObra(o);
			ObjectNode nuevaForma = shapeWithNode.getNode();
			Shape agregado = null;

			if (s.isNew()) {
				agregado = addShape(o, c, s.getDescripcion(), s.getTipo(), s.getEstado(), nuevaForma);
			} else {
				Shape actualizarShape = shapeLogic.findById(s.getId());
				actualizarShape.setDescripcion(s.getDescripcion());
				actualizarShape.setObra(s.getObra());
				actualizarShape.setTipo(s.getTipo());
				actualizarShape.setEstado(s.getEstado());
				agregado = updateShape(actualizarShape, nuevaForma);
			}

			toRet.add(agregado);
		}

		List<Shape> aBorrar = obtenerShapesABorrar(idContrato, toRet);
		for (Shape ab : aBorrar) {
			shapeLogic.remove(ab);
		}

		return toRet;
	}

	public List<ShapeDTO> getAllShapes(Long id) {
		return getEm().createQuery(QUERY_GET_SHAPES_CONTRATOS, ShapeDTO.class).setParameter("contratoId", id)
				.getResultList();
	}

	private Shape updateShape(Shape shape, ObjectNode nuevaForma) {
		Shape s = shapeLogic.updateShape(shape, nuevaForma);

		return s;
	}

	private List<Shape> obtenerShapesABorrar(Long id, Collection<Shape> nuevos) {

		List<Shape> borrar = new ArrayList<>();
		List<Shape> shapesViejos = getStreamAll(Shape.class).where(sh -> sh.getContrato().getId() == id).toList();
		for (Shape sv : shapesViejos) {
			if (!nuevos.contains(sv)) {
				borrar.add(sv);
			}
		}
		return borrar;
	}

	public List<ContratoDNCPDTO> obtenerContratoDNCP(String nroDNCP, String codDNCP) {
		// Validate.notBlank(nroDNCP, "Codigo invalido");
		// Validate.notBlank(codDNCP, "Codigo invalido");
		ArrayList<ContratoDNCPDTO> contratos = new ArrayList<ContratoDNCPDTO>();
		String url = "https://www.contrataciones.gov.py/datos/api/v2/doc/buscadores/contratos";
		String charset = "UTF-8";
		String param1 = nroDNCP;
		String param2 = codDNCP;

		String query = null;
		try {
			if (param1 != null && param2 != null) {
				query = String.format("nro_nombre_licitacion=%s&codigo_contratacion=%s",
						URLEncoder.encode(param1, charset), URLEncoder.encode(param2, charset));

			} else if (param1 != null) {
				query = String.format("nro_nombre_licitacion=%s", URLEncoder.encode(param1, charset));
			} else if (param2 != null) {
				query = String.format("codigo_contratacion=%s", URLEncoder.encode(param2, charset));
			} else {
				System.out.println("No se obtuvo ningun parametro");
				return null;
			}

			URLConnection connection = new URL(url + "?" + query).openConnection();
			// connection.setRequestProperty("Authorization", "Bearer " +
			// config.getString("dncp.token"));

			connection.setRequestProperty("Accept-Charset", charset);
			JsonNode node = om.readTree(connection.getInputStream());
			JsonNode resultados = node.get("@graph").get(0);

			for (int i = 0; i < resultados.get("list").size(); i++) {
				JsonNode contrato = resultados.get("list").get(i);
				ContratoDNCPDTO cd = new ContratoDNCPDTO();
				cd.setIdLlamado(contrato.get("id").asText());
				cd.setNroContratacion(contrato.get("id_llamado").asLong());
				cd.setCodContratacion(contrato.get("codigo_contratacion").asText());
				cd.setFechaContrato(contrato.get("fecha_firma_contrato").asText());
				cd.setMontoAdjudicado(contrato.get("monto_adjudicado").asLong());
				cd.setMoneda(contrato.get("moneda").get("nombre").asText());
				cd.setRuc(contrato.get("proveedor").get("ruc").asText());
				cd.setRazonSocial(contrato.get("proveedor").get("razon_social").asText());
				cd.setEstado(contrato.get("estado").get("nombre").asText());
				cd.setLinkDncp("https://www.contrataciones.gov.py/licitaciones/adjudicacion/contrato/"
						+ cd.getIdLlamado() + ".html");
				contratos.add(cd);
			}

			// ContratoDNCP lp = om.treeToValue(resultado, ContratoDNCP.class);

			return contratos;

		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException("URL Invalida", e);
		}
	}

	@Data
	public static class ContratoDNCPDTO {
		@NotNull
		private Long nroContratacion;

		@NotNull
		private String codContratacion;

		@NotNull
		private String fechaContrato;

		@NotNull
		private long montoAdjudicado;

		@NotNull
		private String ruc;

		@NotNull
		private String razonSocial;

		@NotNull
		private String moneda;

		@NotNull
		private String estado;

		@NotNull
		private String idLlamado;

		@NotNull
		private String linkDncp;
	}

}
