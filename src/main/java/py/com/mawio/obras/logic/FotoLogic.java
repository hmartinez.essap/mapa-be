package py.com.mawio.obras.logic;

import java.io.InputStream;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.com.cds.framework.ejb.BaseLogic;
import py.com.cds.framework.pictshare.PictShareHelper;
import py.com.mawio.obras.model.Foto;

@Stateless
public class FotoLogic extends BaseLogic<Foto> {

	@Inject
	PictShareHelper ps;

	public Foto updateImage(Long id, InputStream image) {
		Foto f = findById(id);
		String hashName = ps.uploadAndGetName(image, "png");
		f.setHash(hashName);
		getEm().persist(f);
		return f;
	}
}
