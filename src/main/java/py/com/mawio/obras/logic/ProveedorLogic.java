package py.com.mawio.obras.logic;

import java.util.Optional;

import javax.ejb.Stateless;

import py.com.cds.framework.ejb.BaseLogic;
import py.com.mawio.be.model.Proveedor;

@Stateless
public class ProveedorLogic extends BaseLogic<Proveedor> {

	public Optional<Proveedor> getByRuc(String ruc) {
		return getStreamAll().where(p -> p.getRuc().equals(ruc)).findAny();
	}

	public Proveedor add(Proveedor p) {

		Optional<Proveedor> aVerificar = getByRuc(p.getRuc());

		if (aVerificar.isPresent()) {
			Proveedor encontrado = aVerificar.get();
			return encontrado;

		}
		return getEm().merge(p);

	}
}
