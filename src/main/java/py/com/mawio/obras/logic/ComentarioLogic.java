package py.com.mawio.obras.logic;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.Validate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;
import lombok.ToString;
import py.com.cds.framework.ejb.BaseLogic;
import py.com.cds.framework.util.Config;
import py.com.mawio.be.model.Comentario;
import py.com.mawio.be.model.Comentario.Estado;
import py.com.mawio.mail.MailSender;
import py.com.mawio.mail.Template;
import py.com.mawio.obras.model.Shape;
import py.com.mawio.security.logic.UserSessionDataHolder;

@Stateless
public class ComentarioLogic extends BaseLogic<Comentario> {

	@Inject
	private UserSessionDataHolder holder;

	@Inject
	private MailSender ms;

	@Inject
	Config config;

	public Optional<Comentario> getByCod(String cod) {
		return getStreamAll().where(c -> c.getCodigo().equals(cod)).findAny();
	}

	public Comentario addComentario(Long shapeId, String mensaje, String email, String telefono, Double longitud,
			Double latitud, String captcha) {

		CaptchaResponse response = validarCaptcha(captcha);
		System.out.println("La respuesta del captcha es " + response.toString());
		Validate.isTrue(response.isSuccess(), "El captcha no es valido");

		Shape s = new Shape();
		s.setId(shapeId);

		Comentario c = new Comentario();
		c.setShape(s);
		c.setComentario(mensaje);
		c.setEmail(email);
		c.setTelefonoContacto(telefono);
		c.setEstado(Estado.SINRESPONDER);
		c.setFechaEmision(new Date());
		c.setLongitud(longitud);
		c.setLatitud(latitud);
		c.setCodigo(RandomStringUtils.randomNumeric(10));

		Map<String, Object> parametros = new HashMap<>();
		parametros.put("comentario", c);

		ms.sendMail(c.getEmail(), Template.EMAIL_MENSAJE_ENVIADO, parametros);

		return add(c);
	}

	public Comentario responderComentario(String comentarioCodigo, String message) {

		Optional<Comentario> encontrado = getByCod(comentarioCodigo);
		Validate.isTrue(encontrado.isPresent(), "Comentario no encontrado");
		//String basePath = config.getString("url", "http://www.mawio.net/#/comentarios/");

		Comentario aResponder = encontrado.get();

		Map<String, Object> parametros = new HashMap<>();
		parametros.put("comentario", aResponder);
		//parametros.put("path", basePath + comentarioCodigo);

		aResponder.setEstado(Estado.RESPONDIDO);
		aResponder.setRespuesta(message);
		aResponder.setFechaRespuesta(new Date());
		aResponder.setUsuarioRespuesta(holder.getUser());
		ms.sendMail(aResponder.getEmail(), Template.EMAIL_MENSAJE_RESPONDIDO, parametros);
		return super.update(aResponder);
	}

	public CaptchaResponse validarCaptcha(String captcha) {

		Validate.notEmpty(captcha, "Captcha invalido");
		String url = "https://www.google.com/recaptcha/api/siteverify";
		String charset = "UTF-8";
		String secret = "6LcufgwUAAAAAIooq0Oxdas01-IkPucVnmtrB71Y";

		URLConnection connection;
		try {
			connection = new URL(url).openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept-Charset", charset);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);

			String query = String.format("secret=%s&response=%s", URLEncoder.encode(secret, charset),
					URLEncoder.encode(captcha, charset));

			try (OutputStream output = connection.getOutputStream()) {
				output.write(query.getBytes(charset));
			}

			try (InputStream response = connection.getInputStream()) {
				return new ObjectMapper().readValue(response, CaptchaResponse.class);
			}

		} catch (IOException e) {
			throw new RuntimeException(e);

		}

	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	@Data
	@ToString
	public static class CaptchaResponse {
		boolean success;
		Date timestamp;
		String hostname;
	}

}
