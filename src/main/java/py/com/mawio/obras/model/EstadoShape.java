package py.com.mawio.obras.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;

@Entity
@Data
@Table(name = "estado_shape")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "EstadoShapeIdGenerator", sequenceName = "estado_shape_id_sequence", allocationSize = 1, initialValue = 1)
public class EstadoShape extends BaseEntity {

	private static final long serialVersionUID = 5152413956870331622L;

	@Id
	@GeneratedValue(generator = "EstadoShapeIdGenerator")
	private long id;

	@NotNull
	@Size(max = 100, min = 3)
	private String icono;

	@NotNull
	@Size(max = 100, min = 3)
	private String tipoLinea;

	@NotNull
	@Size(max = 100, min = 3)
	private String descripcion;

	private String codigoLinea;
}