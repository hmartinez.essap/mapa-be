package py.com.mawio.obras.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;

@Entity
@Data
@Table(name = "tipo_shape")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "TipoShapeIdGenerator", sequenceName = "tipo_shape_id_sequence", allocationSize = 1, initialValue = 1)
public class TipoShape extends BaseEntity {

	private static final long serialVersionUID = -7247478812152802120L;

	@Id
	@GeneratedValue(generator = "TipoShapeIdGenerator")
	private long id;

	@NotNull
	@Size(max = 100, min = 3)
	private String icono;

	@NotNull
	@Size(max = 100, min = 3)
	private String color;

	@NotNull
	@Size(max = 100, min = 3)
	private String descripcion;

}