package py.com.mawio.obras.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.geolatte.geom.C2D;
import org.geolatte.geom.Geometry;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;
import py.com.mawio.be.model.Contrato;

@Entity
@Data
@Table(name = "shape")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "ShapeIdGenerator", sequenceName = "shape_id_sequence", allocationSize = 1, initialValue = 1)
public class Shape extends BaseEntity {

	private static final long serialVersionUID = -703866109493503328L;

	@Id
	@GeneratedValue(generator = "ShapeIdGenerator")
	private long id;

	@JsonIgnore
	@Column(updatable = false, insertable = false)
	private Geometry<C2D> geometry;

	@ManyToOne
	private Contrato contrato;

	@NotNull
	@ManyToOne
	private Obra obra;

	@NotNull
	@ManyToOne
	private TipoShape tipo;

	@NotNull
	@Size(max = 100, min = 3)
	private String descripcion;

	@JsonIgnore
	@OneToMany(mappedBy = "shape", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	private List<Foto> fotos;

	@NotNull
	@ManyToOne
	private EstadoShape estado;
}