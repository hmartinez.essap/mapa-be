package py.com.mawio.obras.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;

@Entity
@Data
@Table(name = "obra")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "ObraIdGenerator", sequenceName = "obra_id_sequence", allocationSize = 1, initialValue = 1)

public class Obra extends BaseEntity {

	private static final long serialVersionUID = -3286716638085804621L;

	public static enum Visible {
		SI, NO
	}

	@Id
	@GeneratedValue(generator = "ObraIdGenerator")
	private long id;

	@NotNull
	@Size(max = 100, min = 3)
	private String area;

	@NotNull
	@Temporal(TemporalType.DATE)
	private Date fechaInicio;

	@Temporal(TemporalType.DATE)
	private Date fechaFin;

	@NotNull
	@Size(max = 100, min = 3)
	private String tipo;

	@NotNull
	@Size(max = 100, min = 3)
	private String descripcion;

	@Enumerated(EnumType.STRING)
	private Visible visible;

}