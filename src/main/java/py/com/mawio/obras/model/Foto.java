package py.com.mawio.obras.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;
import py.com.cds.framework.pictshare.PictSharePictureSerializator;

@Entity
@Data
@Table(name = "foto")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "FotoIdGenerator", sequenceName = "foto_id_sequence", allocationSize = 1, initialValue = 1)
public class Foto extends BaseEntity {

	private static final long serialVersionUID = 3706282308007037734L;

	@Id
	@GeneratedValue(generator = "FotoIdGenerator")
	private long id;

	@NotNull
	@ManyToOne
	@JsonIgnore
	private Shape shape;

	@NotNull
	@Size(max = 100, min = 3)
	@JsonSerialize(using = PictSharePictureSerializator.class)
	private String hash;
}