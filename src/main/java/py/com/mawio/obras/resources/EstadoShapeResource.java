package py.com.mawio.obras.resources;

import java.util.List;

import javax.inject.Inject;
import javax.websocket.server.PathParam;
import javax.ws.rs.Path;

import org.jboss.resteasy.annotations.GZIP;

import io.swagger.annotations.Api;
import py.com.cds.framework.ejb.BaseLogic;
import py.com.cds.framework.rest.BaseResource;
import py.com.mawio.obras.logic.EstadoShapeLogic;
import py.com.mawio.obras.model.EstadoShape;
import py.com.mawio.security.logic.Logged;
import py.com.mawio.security.logic.Logged.Rol;

@Path("estados")
@Api("Consulta sobre estados de shapes")
@GZIP
@Logged(Rol.CARGADOR)
public class EstadoShapeResource extends BaseResource<EstadoShape> {

	@Inject
	private EstadoShapeLogic logic;

	@Override
	@Logged(isPublic = true)
	public EstadoShape get(@PathParam("id") Long id) {
		return super.get(id);
	}

	@Override
	@Logged(isPublic = true)
	public List<EstadoShape> getAll() {
		return super.getAll();
	}

	@Override
	protected BaseLogic<EstadoShape> getBaseBean() {
		return logic;
	}

}
