package py.com.mawio.obras.resources;

import java.io.IOException;
import java.io.InputStream;
import javax.inject.Inject;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import py.com.cds.framework.util.ResteasyUtils;
import py.com.mawio.obras.logic.FotoLogic;
import py.com.mawio.obras.model.Foto;
import py.com.mawio.security.logic.Logged;

@Path("foto")
@Api("Operaciones sobre fotos")
@GZIP
public class FotosResource {

    @Inject
    FotoLogic logic;

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(value = "Actualiza la imagen de un shape", hidden = true)
    @ApiImplicitParams(@ApiImplicitParam(dataType = "java.io.File", name = "image", value = "Nueva imagen", paramType = "formData", required = true))
    public Foto uploadBannerImage(@PathParam("id") @DefaultValue("0") Long id,
            @ApiParam(hidden = true) MultipartFormDataInput image,
            @Context UriInfo info) {

        try (InputStream file = ResteasyUtils.getPart(image, "image")) {
            return logic.updateImage(id, file);
        } catch (IOException io) {
            throw new IllegalArgumentException("The file can't be uploaded");
        }

    }

    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Elimina una foto	", hidden = true)
    @Logged(isPublic = true)
    public Foto delete(@PathParam("id") @NotNull @Min(0) Long id) {

        return logic.remove(id);
    }

}
