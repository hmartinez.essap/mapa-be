package py.com.mawio.obras.resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.annotations.cache.Cache;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import py.com.cds.framework.util.ResteasyUtils;
import py.com.mawio.obras.logic.ShapeLogic;
import py.com.mawio.obras.logic.ShapeLogic.ShapeDTO;
import py.com.mawio.obras.logic.ShapeLogic.ShapeInfoDTO;
import py.com.mawio.obras.model.Foto;
import py.com.mawio.obras.model.Obra;
import py.com.mawio.security.logic.Logged;
import py.com.mawio.security.logic.Logged.Rol;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("shape")
@Api("Operaciones sobre shape")
@GZIP
@Cache(maxAge = 86400)
public class ShapesResource {

	@Inject
	ShapeLogic logic;

	@GET
	@Path("{id}/info")
	@ApiOperation("Retorna la informacion del shape")
	@Logged(isPublic = true)
	public ShapeInfoDTO getShapeInfo(@PathParam("id") Long id) {

		return logic.getInfo(id);

	}

	@POST
	@Logged({ Rol.CARGADOR })
	@Path("/{shape:[0-9][0-9]*}/image")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@ApiOperation(value = "Carga una imagen a una obra", hidden = true)
	@ApiImplicitParams(@ApiImplicitParam(dataType = "java.io.File", name = "image", value = "Nueva imagen", paramType = "formData", required = true))
	public Foto addImage(@PathParam("shape") @DefaultValue("0") @ApiParam(value = "Identifier of the place") Long id,
			@ApiParam(hidden = true) MultipartFormDataInput image, @Context UriInfo info) {

		try (InputStream file = ResteasyUtils.getPart(image, "image")) {
			return logic.addImage(id, file);
		} catch (IOException io) {
			throw new IllegalArgumentException("The file can't be uploaded");
		}

	}

	@GET
	@Path("{id}/images")
	@ApiOperation("Retorna todas las fotos pertenecientes a un shape")
	@Logged(isPublic = true)
	public List<Foto> getAllImages(@PathParam("id") @ApiParam("Identificador del shape") @NotNull Long id) {

		return logic.getAllImages(id);

	}

	@GET
	@Path("all")
	@ApiOperation(value = "Obtiene una lista de shapes por filtros")
	@Logged(isPublic = true)
	public List<ShapeDTO> getFiltered(@QueryParam("dpto") @ApiParam(value = "Identificador del dpto") String dpto,
			@QueryParam("distrito") @ApiParam(value = "Identificador del distrito") String distrito,
			@QueryParam("localidad") @ApiParam(value = "Identificador de la localidad") String barloc,
			@QueryParam("fechaInicioObra") @ApiParam(value = "Fecha del inicio de la obra") String fechaInicioObra,
			@QueryParam("fechaFinObra") @ApiParam(value = "Fecha del fin de la obra") String fechaFinObra,
			@QueryParam("montoInferior") @ApiParam(value = "Monto inferior del contrato") Long montoInferior,
			@QueryParam("montoSuperior") @ApiParam(value = "Monto superior del contrato") Long montoSuperior,
			@QueryParam("proveedorId") @ApiParam(value = "Identificador del proveedor") Long proveedorId,
			@QueryParam("fechaInicioContrato") @ApiParam(value = "Fecha del inicio del contrato") String fechaInicioContrato,
			@QueryParam("fechaFinContrato") @ApiParam(value = "Fecha del fin del contrato") String fechaFinContrato,
			@QueryParam("offset") @ApiParam(value = "Offset") Integer offset,
			@QueryParam("limit") @ApiParam(value = "Limit") Integer limit,
			@QueryParam("order") @ApiParam(value = "Order") String order) {

		return logic.getFiltered(dpto, distrito, barloc, fechaInicioObra, fechaFinObra, montoInferior, montoSuperior,
				proveedorId, fechaInicioContrato, fechaFinContrato, offset, limit, order);

	}

	@GET
	@Path("all/csv")
	@ApiOperation(value = "Obtiene una lista de obras de shapes por filtros")
	@Logged(isPublic = true)
	@Produces({ "text/csv" })
	public Response getFilteredCompleto(@QueryParam("dpto") @ApiParam(value = "Identificador del dpto") String dpto,
			@QueryParam("distrito") @ApiParam(value = "Identificador del distrito") String distrito,
			@QueryParam("localidad") @ApiParam(value = "Identificador de la localidad") String barloc,
			@QueryParam("fechaInicioObra") @ApiParam(value = "Fecha del inicio de la obra") String fechaInicioObra,
			@QueryParam("fechaFinObra") @ApiParam(value = "Fecha del fin de la obra") String fechaFinObra,
			@QueryParam("montoInferior") @ApiParam(value = "Monto inferior del contrato") Long montoInferior,
			@QueryParam("montoSuperior") @ApiParam(value = "Monto superior del contrato") Long montoSuperior,
			@QueryParam("proveedorId") @ApiParam(value = "Identificador del proveedor") Long proveedorId,
			@QueryParam("fechaInicioContrato") @ApiParam(value = "Fecha del inicio del contrato") String fechaInicioContrato,
			@QueryParam("fechaFinContrato") @ApiParam(value = "Fecha del fin del contrato") String fechaFinContrato,
			@QueryParam("offset") @ApiParam(value = "Offset") Integer offset,
			@QueryParam("limit") @ApiParam(value = "Limit") Integer limit,
			@QueryParam("order") @ApiParam(value = "Order") String order) throws IOException {

		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper.schemaFor(Obra.class).withHeader();

		List<Obra> obras = logic.getFilteredCompleto(dpto, distrito, barloc, fechaInicioObra, fechaFinObra,
				montoInferior, montoSuperior, proveedorId, fechaInicioContrato, fechaFinContrato, offset, limit, order);
		String csv = mapper.writer(schema).writeValueAsString(obras);
		return Response.ok(csv).header("Content-Disposition", "attachment; filename=" + "obras.csv").build();
	}

	@GET
	@Path("all/json")
	@ApiOperation(value = "Obtiene una lista de obras de shapes por filtros")
	@Logged(isPublic = true)
	@Produces({ "text/json" })
	public Response getFilteredCompletoJson(@QueryParam("dpto") @ApiParam(value = "Identificador del dpto") String dpto,
			@QueryParam("distrito") @ApiParam(value = "Identificador del distrito") String distrito,
			@QueryParam("localidad") @ApiParam(value = "Identificador de la localidad") String barloc,
			@QueryParam("fechaInicioObra") @ApiParam(value = "Fecha del inicio de la obra") String fechaInicioObra,
			@QueryParam("fechaFinObra") @ApiParam(value = "Fecha del fin de la obra") String fechaFinObra,
			@QueryParam("montoInferior") @ApiParam(value = "Monto inferior del contrato") Long montoInferior,
			@QueryParam("montoSuperior") @ApiParam(value = "Monto superior del contrato") Long montoSuperior,
			@QueryParam("proveedorId") @ApiParam(value = "Identificador del proveedor") Long proveedorId,
			@QueryParam("fechaInicioContrato") @ApiParam(value = "Fecha del inicio del contrato") String fechaInicioContrato,
			@QueryParam("fechaFinContrato") @ApiParam(value = "Fecha del fin del contrato") String fechaFinContrato,
			@QueryParam("offset") @ApiParam(value = "Offset") Integer offset,
			@QueryParam("limit") @ApiParam(value = "Limit") Integer limit,
			@QueryParam("order") @ApiParam(value = "Order") String order) throws IOException {

		List<Obra> obras = logic.getFilteredCompleto(dpto, distrito, barloc, fechaInicioObra, fechaFinObra,
				montoInferior, montoSuperior, proveedorId, fechaInicioContrato, fechaFinContrato, offset, limit, order);
		return Response.ok(obras).header("Content-Disposition", "attachment; filename=" + "obras.json").build();
	}

}
