package py.com.mawio.obras.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.annotations.cache.Cache;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.com.cds.framework.ejb.BaseLogic;
import py.com.cds.framework.rest.BaseResource;
import py.com.mawio.be.model.Proveedor;
import py.com.mawio.obras.logic.ProveedorLogic;
import py.com.mawio.security.logic.Logged;
import py.com.mawio.security.logic.Logged.Rol;

@Path("proveedores")
@Api("Operaciones sobre proveedores")
@GZIP
@Cache(maxAge = 86400)
@Logged(Rol.ADMINISTRADOR)
public class ProveedoresResource extends BaseResource<Proveedor> {

	@Inject
	private ProveedorLogic logic;

	@Override
	@Logged(isPublic = true)
	public List<Proveedor> getAll() {
		return super.getAll();
	}

	@POST
	@ApiOperation(value = "Permite agregar un nuevo proveedor", hidden = true)
	public Proveedor add(Proveedor proveedor) {
		return logic.add(proveedor);
	}
	@Override
	protected BaseLogic<Proveedor> getBaseBean() {

		return logic;
	}
}
