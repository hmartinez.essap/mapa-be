package py.com.mawio.obras.resources;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.GZIP;

import com.fasterxml.jackson.databind.node.ObjectNode;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import py.com.cds.framework.ejb.BaseLogic;
import py.com.cds.framework.rest.BaseResource;
import py.com.mawio.obras.logic.ContratoLogic;
import py.com.mawio.obras.logic.ContratoLogic.ContratoDNCPDTO;
import py.com.mawio.obras.logic.ObraLogic;
import py.com.mawio.obras.logic.ObraLogic.ShapeWithNode;
import py.com.mawio.obras.logic.ShapeLogic;
import py.com.mawio.obras.logic.ShapeLogic.ShapeDTO;
import py.com.mawio.obras.model.EstadoShape;
import py.com.mawio.obras.model.Obra;
import py.com.mawio.obras.model.Shape;
import py.com.mawio.obras.model.TipoShape;
import py.com.mawio.security.logic.Logged;
import py.com.mawio.security.logic.Logged.Rol;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@GZIP
@Path("obras")
@Api("Operaciones sobre obras")
@Logged({ Rol.ADMINISTRADOR, Rol.CARGADOR, Rol.RESPONDEDOR })
public class ObrasResource extends BaseResource<Obra> {

	@Inject
	private ObraLogic logic;

	@Inject
	private ShapeLogic shapeLogic;

	@Inject
	private ContratoLogic contratoLogic;

	@Override
	protected BaseLogic<Obra> getBaseBean() {

		return logic;
	}

	@GET
	@Path("{id}/shapes")
	@ApiOperation("Retorna todos los shapes de una obra con contratos")
	@Logged(isPublic = true)
	public List<ShapeDTO> getShapes(@PathParam("id") @ApiParam("Identificador de la obra") @NotNull Long id,
			@QueryParam("dpto") @ApiParam(value = "Identificador del dpto") String dpto,
			@QueryParam("distrito") @ApiParam(value = "Identificador del distrito") String distrito,
			@QueryParam("localidad") @ApiParam(value = "Identificador de la localidad") String barloc,
			@QueryParam("fechaInicioObra") @ApiParam(value = "Fecha del inicio de la obra") String fechaInicioObra,
			@QueryParam("fechaFinObra") @ApiParam(value = "Fecha del fin de la obra") String fechaFinObra,
			@QueryParam("montoInferior") @ApiParam(value = "Monto inferior del contrato") Long montoInferior,
			@QueryParam("montoSuperior") @ApiParam(value = "Monto superior del contrato") Long montoSuperior,
			@QueryParam("proveedorId") @ApiParam(value = "Identificador del proveedor") Long proveedorId,
			@QueryParam("fechaInicioContrato") @ApiParam(value = "Fecha del inicio del contrato") String fechaInicioContrato,
			@QueryParam("fechaFinContrato") @ApiParam(value = "Fecha del fin del contrato") String fechaFinContrato,
			@QueryParam("offset") @ApiParam(value = "Offset") Integer offset,
			@QueryParam("limit") @ApiParam(value = "Limit") Integer limit,
			@QueryParam("order") @ApiParam(value = "Order") String order) {

		return shapeLogic.getFilteredByObra(id, dpto, distrito, barloc, fechaInicioObra, fechaFinObra, montoInferior,
				montoSuperior, proveedorId, fechaInicioContrato, fechaFinContrato, offset, limit, order);

	}

	@GET
	@Path("/contratosDNCP")
	@Logged(isPublic = true)
	public List<ContratoDNCPDTO> obtenerContratoDNCP(@QueryParam("nroDNCP") String nroDNCP,
			@QueryParam("codDNCP") String codDNCP) {
		return contratoLogic.obtenerContratoDNCP(nroDNCP, codDNCP);
	}

	@GET
	@Path("{id}/allshapes")
	@ApiOperation("Retorna todos los shapes de una obra")
	@Logged(isPublic = true)
	public List<ShapeDTO> getShapesConContratos(
			@PathParam("id") @ApiParam("Identificador de la obra") @NotNull Long id) {

		return logic.getAllShapes(id);

	}

	@Override
	@Logged(isPublic = true)
	public Obra get(@PathParam("id") Long id) {
		return super.get(id);
	}

	@POST
	@Path("{id}/shapes")
	@ApiOperation(value = "Agrega una lista de shapes a una obra", hidden = true)
	public List<Shape> addShape(@PathParam("id") @ApiParam("Identificador de la obra") @NotNull Long id,
			List<PeticionParaAgregarShape> parametro) {

		List<ShapeWithNode> datos = new ArrayList<>();
		for (PeticionParaAgregarShape request : parametro) {
			Shape s = new Shape();
			if (request.getId() != null)
				s.setId(request.getId());
			s.setDescripcion(request.getDescripcion());
			s.setTipo(request.getTipo());
			s.setEstado(request.getEstado());
			datos.add(new ShapeWithNode(s, request.getGeometry()));
		}

		return logic.updateShapes(id, datos);
	}

	@Data
	public static class PeticionParaAgregarShape {
		Long id;
		String descripcion;
		TipoShape tipo;
		EstadoShape estado;
		ObjectNode geometry;
	}

}
