package py.com.mawio.obras.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.annotations.cache.Cache;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.com.mawio.be.model.Barrio;
import py.com.mawio.obras.logic.BarrioLogic;
import py.com.mawio.security.logic.Logged;

@Path("barrio")
@Api("Consulta sobre un barrio")
@GZIP
@Cache(maxAge = 86400)
@Logged(isPublic = true)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BarriosResource {

	@Inject
	private BarrioLogic logic;

	@GET
	@ApiOperation(value = "Obtiene todos los distritos de un departamento")
	@Logged(isPublic = true)
	public List<Barrio> get(@QueryParam("search[value]") @DefaultValue("") String globalSearch) {

		return logic.getFiltered(globalSearch);

	}

}
