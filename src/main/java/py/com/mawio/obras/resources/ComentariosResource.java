package py.com.mawio.obras.resources;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.jboss.resteasy.annotations.GZIP;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import py.com.cds.framework.ejb.BaseLogic;
import py.com.cds.framework.rest.BaseResource;
import py.com.mawio.be.model.Comentario;
import py.com.mawio.obras.logic.ComentarioLogic;
import py.com.mawio.security.logic.Logged;
import py.com.mawio.security.logic.Logged.Rol;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@GZIP
@Path("comentarios")
@Api(value = "Operaciones sobre comentarios", hidden = true)
@Logged({ Rol.ADMINISTRADOR, Rol.RESPONDEDOR })
public class ComentariosResource extends BaseResource<Comentario> {
	@Inject
	private ComentarioLogic comentarioLogic;

	@Override
	protected BaseLogic<Comentario> getBaseBean() {
		return comentarioLogic;
	}

	
	@POST
	@Path("/public")
	@Logged(isPublic = true)
	@ApiOperation("Inserta un nuevo comentario")
	public Comentario addComentario(ComentarioDTO c) {

		return comentarioLogic.addComentario(c.getShapeId(), c.getMensaje(), c.getEmail(), c.getTelefono(),
				c.getLongitud(), c.getLatitud(), c.getCaptcha());
	}

	@GET
	@Path("/byCodigo/{cod}")
	@ApiOperation("Retorna todos los comentarios a partir de un codigo publico")
	@Logged(isPublic = true)
	public Comentario getByCodigo(@PathParam("cod") @ApiParam("Codigo del comentario") @NotNull String cod) {
		return comentarioLogic.getByCod(cod).orElse(null);
	}

	@POST
	@Path("/byCodigo/{cod}/responder")
	@ApiOperation("Retorna todos los comentarios a partir de un codigo publico")
	@Logged(Logged.Rol.RESPONDEDOR)
	public Comentario responer(@PathParam("cod") String comentarioCodigo, @NotNull @Valid ResponderData data) {
		return comentarioLogic.responderComentario(comentarioCodigo, data.getMessage());
	}

	@Data
	public static class ResponderData {

		@NotBlank
		@Size(min = 3)
		String message;
	}

	@Data
	public static class ComentarioDTO {

		@NotNull
		Long shapeId;

		@NotBlank
		String mensaje;

		@Email
		@NotBlank
		String email;

		String telefono;

		@NotNull
		Double longitud;

		@NotNull
		Double latitud;

		@NotBlank
		String captcha;
	}
}
