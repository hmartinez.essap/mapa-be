package py.com.mawio.obras.resources;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.annotations.GZIP;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import py.com.cds.framework.rest.DataTablesHelper;
import py.com.cds.framework.rest.DataTablesHelper.QueryExtracted;
import py.com.cds.framework.util.DataWithCount;
import py.com.mawio.be.model.Contrato;
import py.com.mawio.obras.logic.ContratoLogic;
import py.com.mawio.obras.logic.ObraLogic;
import py.com.mawio.obras.logic.ObraLogic.ShapeWithNode;
import py.com.mawio.obras.logic.ShapeLogic.ShapeDTO;
import py.com.mawio.obras.model.Shape;
import py.com.mawio.obras.resources.ObrasResource.PeticionParaAgregarShape;
import py.com.mawio.security.logic.Logged;
import py.com.mawio.security.logic.Logged.Rol;

@Path("obra/{id}/contrato")
@Api("Operaciones sobre contratos")
@GZIP
@Logged({ Rol.ADMINISTRADOR, Rol.CARGADOR })
public class ContratosResource {

	@PathParam("id")
	@NotNull
	Long obraId;

	@Inject
	ObraLogic logic;

	@Inject
	ContratoLogic contratoLogic;

	@Inject
	DataTablesHelper helper;

	@Context
	UriInfo uriInfo;

	@POST
	@ApiOperation(value = "Agrega un contrato", hidden = true)
	public Contrato addContrato(@NotNull Contrato contrato) {

		return logic.addContrato(obraId, contrato);
	}

	@GET
	@Logged(isPublic = true)
	public DataWithCount<Contrato> getContratos(@QueryParam("draw") @DefaultValue("0") Long draw,
			@QueryParam("start") @DefaultValue("0") Integer start,
			@QueryParam("length") @DefaultValue("0") Integer length,
			@QueryParam("search[value]") @DefaultValue("") String globalSearch) {

		QueryExtracted qe = helper.extractData(uriInfo.getQueryParameters());

		DataWithCount<Contrato> data = logic.getContratos(obraId, start, length, qe.getOrders(), qe.getValues(),
				qe.getProperties(), StringUtils.isEmpty(globalSearch) ? null : globalSearch);
		data.setDraw(draw);
		return data;
	}

	@GET
	@Path("/{idContrato}")
	@Logged(isPublic = true)
	public Contrato getContratoById(@PathParam("idContrato") Long idContrato) {

		return contratoLogic.findById(idContrato);
	}

	@PUT
	@Path("/{idContrato}")
	@ApiOperation(value = "Actualiza un contrato", hidden = true)
	public Contrato updateContratoById(@PathParam("idContrato") Long idContrato, Contrato contrato) {

		contrato.setId(idContrato);
		return contratoLogic.update(contrato);
	}

	@DELETE
	@Path("/{idContrato}")
	@ApiOperation(value = "Borra un contrato", hidden = true)
	public Contrato borrarContratoById(@PathParam("idContrato") Long idContrato) {

		return contratoLogic.remove(idContrato);
	}

	@GET
	@Path("{idContrato}/allShapes")
	@ApiOperation("Retorna todos los shapes de un contrato")
	@Logged(isPublic = true)
	public List<ShapeDTO> getShapesConContratos(
			@PathParam("idContrato") @ApiParam("Identificador del contrato") @NotNull Long id) {

		return contratoLogic.getAllShapes(id);

	}

	@POST
	@Path("/{idContrato}/shapes")
	@ApiOperation(value = "Agrega un shape a un contrato", hidden = true)
	public List<Shape> addShape(
			@PathParam("idContrato") @ApiParam("Identificador del contrato") @NotNull Long idContrato,
			List<PeticionParaAgregarShape> parametro) {

		List<ShapeWithNode> datos = new ArrayList<>();
		for (PeticionParaAgregarShape request : parametro) {
			Shape s = new Shape();
			if (request.getId() != null) {
				s.setId(request.getId());
			}
			s.setDescripcion(request.getDescripcion());
			s.setTipo(request.getTipo());
			s.setEstado(request.getEstado());
			datos.add(new ShapeWithNode(s, request.getGeometry()));
		}

		return contratoLogic.updateShapes(idContrato, obraId, datos);
	}
}
