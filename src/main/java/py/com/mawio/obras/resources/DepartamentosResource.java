package py.com.mawio.obras.resources;

import java.util.List;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.annotations.cache.Cache;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import py.com.mawio.be.model.Barrio;
import py.com.mawio.be.model.Departamento;
import py.com.mawio.obras.logic.DepartamentoLogic;
import py.com.mawio.obras.logic.DepartamentoLogic.DistritoDTO;
import py.com.mawio.security.logic.Logged;

@Path("departamento")
@Api("Operaciones sobre departamentos")
@Logged(isPublic = true)
@GZIP
@Cache(maxAge = 86400)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DepartamentosResource {

	@Inject
	private DepartamentoLogic logic;

	@GET
	@Path("/all")
	@Logged(isPublic = true)
	public List<Departamento> getAll() {

		return logic.getAll();
	}

	@GET
	@Path("{cod}/distritos")
	@ApiOperation("Retorna todos los distritos de un departamento")
	@Logged(isPublic = true)
	public List<DistritoDTO> getAllDistritos(
			@PathParam("cod") @ApiParam("Codigo del departamento") @NotNull String cod) {

		return logic.getAllDistritos(cod);

	}

	@GET
	@Path("{codDpto}/distrito/{codDistrito}/barrios")
	@ApiOperation("Retorna todos los barrios de un departamento")
	@Logged(isPublic = true)
	public List<Barrio> getAllBarrios(
			@PathParam("codDpto") @ApiParam("Codigo del departamento") @NotNull String codDpto,
			@PathParam("codDistrito") @ApiParam("Codigo del distrito") @NotNull String codDistrito) {

		return logic.getAllBarrios(codDpto, codDistrito);

	}

}
