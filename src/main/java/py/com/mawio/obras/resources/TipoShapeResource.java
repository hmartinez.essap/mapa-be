package py.com.mawio.obras.resources;

import java.util.List;

import javax.inject.Inject;
import javax.websocket.server.PathParam;
import javax.ws.rs.Path;

import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.annotations.cache.Cache;

import io.swagger.annotations.Api;
import py.com.cds.framework.ejb.BaseLogic;
import py.com.cds.framework.rest.BaseResource;
import py.com.mawio.obras.logic.TipoShapeLogic;
import py.com.mawio.obras.model.TipoShape;
import py.com.mawio.security.logic.Logged;
import py.com.mawio.security.logic.Logged.Rol;

@Path("tipos")
@Api("Consulta sobre tipo de shapes")
@GZIP
@Cache(maxAge = 86400)
@Logged(Rol.CARGADOR)
public class TipoShapeResource extends BaseResource<TipoShape> {

	@Inject
	private TipoShapeLogic logic;

	@Override
	@Logged(isPublic = true)
	public TipoShape get(@PathParam("id") Long id) {
		return super.get(id);
	}

	@Override
	@Logged(isPublic = true)
	public List<TipoShape> getAll() {
		return super.getAll();
	}

	@Override
	protected BaseLogic<TipoShape> getBaseBean() {
		return logic;
	}

}
