package py.com.mawio.security.logic;

import static org.torpedoquery.jpa.Torpedo.from;
import static org.torpedoquery.jpa.Torpedo.select;
import static org.torpedoquery.jpa.Torpedo.where;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.lang3.Validate;

import py.com.cds.framework.ejb.BaseLogic;
import py.com.mawio.security.logic.Logged.Rol;
import py.com.mawio.security.model.Usuario;
import py.com.mawio.security.model.Usuario.Estado;
import py.com.mawio.security.model.UsuarioRol;
import py.com.mawio.security.model.UsuarioRol.PK;

@Stateless
public class UsuarioLogic extends BaseLogic<Usuario> {

	/**
	 * Retorna un usuario de acuerdo a su nombre de usuario y contraseña.
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public Usuario getByUsernameAndPassword(String username, String password) {

		Validate.notNull(username, "Usuario nulo");
		Validate.notNull(password, "password nulo");

		String md5 = MD5(password);
		Usuario from = from(Usuario.class);
		where(from.getUsername()).eq(username)
				.and(from.getContrasenha())
				.eq(md5)
				.and(from.getEstado())
				.eq(Estado.ACTIVO);

		List<Usuario> result = select(from).list(getEm());

		if (result.isEmpty())
			throw new IllegalArgumentException("Usuario o contraseña invalidos");
		return result.get(0);

	}

	/**
	 * Retorna el hash MD5 de una cadena.
	 * 
	 * @param md5
	 *            cadena no nula.
	 * @return la cadena en formato md5
	 */
	private String MD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (byte element : array) {
				sb.append(Integer.toHexString((element & 0xFF) | 0x100).substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Cambia el password de un usuario, y lo activa
	 * 
	 * @param usuario
	 * @param newPass
	 */
	public void changePassword(Usuario usuario, String newPass) {

		Usuario u = getEm().getReference(Usuario.class, usuario.getId());
		u.setContrasenha(MD5(newPass));
		super.update(u);
	}

	public void inactivarUser(long userId) {
		Usuario u = findById(userId);
		u.setEstado(Estado.INACTIVO);
		super.update(u);
	}

	public void activarUser(long userId) {
		Usuario u = findById(userId);
		u.setEstado(Estado.ACTIVO);
		super.update(u);
	}

	public boolean hasAnyRole(Usuario u, Rol[] roles) {

		//@formatter:off
		Long l = (Long) getEm().createQuery(""
				+ "	SELECT 	"
				+ "		count (*) "
				+ "	FROM Usuario u"
				+ "	JOIN u.roles r"
				+ "	WHERE "
				+ "			u = :user"
				+ "	AND		r.rol IN :roles")
				.setParameter("user", u)
				.setParameter("roles", Arrays.asList(roles))
				.getSingleResult();
		//@formatter:on
		return l > 0L;
	}

	public Usuario update(long id, String username, String nombre, String dependencia, String correo, Rol[] rol) {
		Usuario u = findById(id);
		setPropertiess(username, nombre, dependencia, correo, u);

		for (Rol r : rol) {
			UsuarioRol ur = new UsuarioRol();
			ur.setPk(new PK(u, r));
			ur.setRol(r);
			if (u.getRoles().add(ur)) {
				System.out.println("Adding " + ur.getPk().getRol());
				getEm().persist(ur);
			}
		}

		List<Rol> asArray = Arrays.asList(rol);
		List<UsuarioRol> aBorrar = new ArrayList<>();
		for (UsuarioRol ur : u.getRoles()) {
			if (!asArray.contains(ur.getPk().getRol())) {
				System.out.println("Removing " + ur.getPk().getRol());
				getEm().remove(ur);
				aBorrar.add(ur);
			}
		}
		aBorrar.forEach(u.getRoles()::remove);

		return super.update(u);
	}

	public Usuario addWithPasssword(String username, String nombre, String dependencia, String correo,
			String contrasenha, Rol[] rol) {

		Validate.notEmpty(rol, "No se puede crear un user sin roles");
		Usuario u = new Usuario();
		setPropertiess(username, nombre, dependencia, correo, u);
		u.setContrasenha(MD5(contrasenha));
		getEm().persist(u);

		for (Rol r : rol) {
			UsuarioRol ur = new UsuarioRol();
			ur.setPk(new PK(u, r));
			getEm().persist(ur);
		}

		return u;
	}

	protected void setPropertiess(String username, String nombre, String dependencia, String correo, Usuario u) {
		u.setUsername(username);
		u.setNombre(nombre);
		u.setDependencia(dependencia);
		u.setCorreo(correo);
	}
}
