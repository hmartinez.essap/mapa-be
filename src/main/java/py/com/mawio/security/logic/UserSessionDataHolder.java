package py.com.mawio.security.logic;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import lombok.AccessLevel;
import lombok.Setter;
import py.com.cds.framework.util.Config;
import py.com.mawio.security.model.Usuario;

@SessionScoped
public class UserSessionDataHolder implements Serializable {

	private static final long serialVersionUID = 9000522619431456058L;

	@Setter(AccessLevel.PUBLIC)
	Usuario user;

	@Inject
	UsuarioLogic usuarioLogic;

	@Inject
	Config config;

	public Usuario getUser() {

		if (user == null && config.isDevel()) {
			Usuario dePruebas = new Usuario();
			dePruebas.setId(1000L);
			dePruebas.setNombre("ADMIN");
			dePruebas.setUsername("ADMIN");
			user = dePruebas;
		}
		return user;
	}

}
