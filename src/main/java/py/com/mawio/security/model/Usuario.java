package py.com.mawio.security.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.jpa.BaseEntity;

@Entity
@Data
@Table(name = "usuario")
@EqualsAndHashCode(callSuper = true, of = "id")
@SequenceGenerator(name = "UsuarioIdGenerator", sequenceName = "usuario_id_sequence")
public class Usuario extends BaseEntity {

	private static final long serialVersionUID = -6823571831844219152L;

	public static enum Estado {
		ACTIVO, INACTIVO
	}

	@Id
	@GeneratedValue(generator = "UsuarioIdGenerator")
	private long id;

	@NotNull
	@Size(max = 100, min = 3)
	private String username;

	@NotNull
	@Size(max = 100, min = 3)
	private String nombre;

	@NotNull
	@Size(max = 100, min = 3)
	private String dependencia;

	@NotNull
	@Size(max = 100, min = 3)
	@Email
	private String correo;

	@JsonIgnore
	private String contrasenha;

	@NotNull
	@Enumerated(EnumType.STRING)
	private Estado estado = Estado.ACTIVO;

	@OneToMany(mappedBy = "usuario", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	private Set<UsuarioRol> roles;

}
