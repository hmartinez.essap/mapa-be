package py.com.mawio.security.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import py.com.mawio.security.logic.Logged.Rol;

@Entity
@Data
@Table(name = "usuario_rol")
@EqualsAndHashCode(of = "pk")
public class UsuarioRol {

	@NotNull
	@EmbeddedId
	@JsonIgnore
	private PK pk;

	@Embeddable
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	@ToString(of = "rol")
	public static class PK implements Serializable {

		private static final long serialVersionUID = 4839570479501149355L;

		@NotNull
		@ManyToOne
		@JoinColumn(name = "user_id")
		private Usuario usuario;

		@NotNull
		@Enumerated(EnumType.STRING)
		private Rol rol;
	}

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	private Usuario usuario;

	@Enumerated(EnumType.STRING)
	@Column(insertable = false, updatable = false)
	private Rol rol;
}
