package py.com.mawio.security.service;

import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;
import org.hibernate.validator.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import py.com.cds.framework.rest.BaseResource;
import py.com.cds.framework.util.DataWithCount;
import py.com.mawio.security.logic.Logged;
import py.com.mawio.security.logic.Logged.Rol;
import py.com.mawio.security.logic.UserSessionDataHolder;
import py.com.mawio.security.logic.UsuarioLogic;
import py.com.mawio.security.model.Usuario;

@Api("Usuarios")
@Path("/usuario")
@Logged(Rol.ADMINISTRADOR)
public class UsuarioService extends BaseResource<Usuario> {

    @Inject
    private UsuarioLogic logic;

    @Inject
    private UserSessionDataHolder holder;

    @Override
    protected UsuarioLogic getBaseBean() {

        return logic;
    }

    @POST
    @ApiOperation(value = "Agrega un usuario", hidden = true)
    public Usuario add(@NotNull @Valid AddUsuarioData entity) {

        Validate.notBlank(entity.getPassword(), "Contrasenha invalida");
        return logic.addWithPasssword(entity.getUsername(), entity.getNombre(),
                entity.getDependencia(), entity.getCorreo(),
                entity.getPassword(), entity.getRol());
    }

    @PUT
    @Path("/{id}")
    @ApiOperation(value = "Actualiza un usuario", hidden = true)
    public Usuario update(@PathParam("id") long userId,
            @NotNull @Valid AddUsuarioData entity) {

        Validate.isTrue(userId != -1000,
                "La cuenta de administrador no puede cambiar");
        return logic.update(userId, entity.getUsername(), entity.getNombre(),
                entity.getDependencia(), entity.getCorreo(), entity.getRol());
    }

    @POST
    @Path("/changePass")
    @ApiOperation(value = "Cambia una contraseña", hidden = true)
    @Logged({ Rol.ADMINISTRADOR, Rol.CARGADOR, Rol.RESPONDEDOR })
    public void changeMyPass(@NotNull @Valid ChangePassData request) {

        logic.changePassword(holder.getUser(), request.getNewPass());
    }

    @Data
    @EqualsAndHashCode(callSuper = true)
    @JsonIgnoreProperties(value = { "estado", "roles" }, ignoreUnknown = true)
    public static class AddUsuarioData extends Usuario {

        private static final long serialVersionUID = 7812512302413039804L;

        @NotNull
        @Size(min = 1, max = 3)
        private Rol[] rol;

        @Size(min = 5, max = 30)
        private String password;

    }

    @Override
    @GET
    @ApiOperation(hidden = true, value = "Obtiene una lista paginada de entidades", notes = "Esto respeta el API de jquery datatables, para más detalles ver https://www.datatables.net/examples/server_side/simple.html")
    public DataWithCount<Usuario> get(
            @QueryParam("draw") @DefaultValue("0") Long draw,
            @QueryParam("start") @DefaultValue("0") Integer start,
            @QueryParam("length") @DefaultValue("0") Integer length,
            @QueryParam("search[value]") @DefaultValue("") String globalSearch) {

        return super.get(draw, start, length, globalSearch);

    }

    @Override
    @GET
    @Path("/all")
    @ApiOperation(value = "Obtiene todas las entidades", hidden = true)
    public List<Usuario> getAll() {

        return super.getAll();
    }

    @Override
    @GET
    @Path("{id}")
    @ApiOperation(value = "Recupera una entidad por su identificador", hidden = true)
    public Usuario get(
            @PathParam("id") @ApiParam("Identificador del usuario") @NotNull Long id) {

        return super.get(id);
    }

    @Override
    @GET
    @Path("/schema")
    @ApiOperation(hidden = true, value = "Retorna un json-schema de la entidad", notes = "Este esquema se puede utilizar para definir las validaciones.")
    public Response getSchema() {

        return super.getSchema();
    }

    @Data
    public static class ChangePassData {

        @NotBlank
        @Size(min = 5, max = 30)
        String newPass;

    }

}
