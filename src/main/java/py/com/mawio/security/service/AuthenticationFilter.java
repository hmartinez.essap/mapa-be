package py.com.mawio.security.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.Optional;
import javax.annotation.Priority;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import py.com.mawio.security.logic.Logged;
import py.com.mawio.security.logic.Logged.Rol;
import py.com.mawio.security.logic.SessionTokenInfo;
import py.com.mawio.security.logic.UserSessionDataHolder;
import py.com.mawio.security.logic.UsuarioLogic;
import py.com.mawio.security.model.Usuario;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

	@Inject
	SessionTokenInfo tokenInfo;

	@Inject
	UserSessionDataHolder dataHolder;

	@Inject
	UsuarioLogic logic;

	@Context
	ResourceInfo resourceInfo;

	@Context
	HttpServletRequest httpRequest;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		UriInfo info = requestContext.getUriInfo();

		if (info.getPath().equals("/authentication") || info.getPath().equals("/swagger.json")) {
			return;
		}

		try {

			// Obtenemos el metodo y vemos si puede usar ?
			Method theMethod = resourceInfo.getResourceMethod();

			Optional<Logged> anot = getAnnotation();
			if (!anot.isPresent()) {
				System.err.println("Método sin roles requeridos encontrado " + theMethod.toString());
				abort(requestContext);
				return;
			}

			if (anot.get().isPublic()) {
				if (!tokenInfo.validateIp(httpRequest.getRemoteAddr())) {
					requestContext.abortWith(Response.status(429).build());
					return;
				} else {
					return;
				}
			}

			// Validate the token
			Usuario user = tokenInfo.validate(getToken(requestContext, info));
			// Guardar el usuario para información de auditoria
			dataHolder.setUser(user);

			if (!userHasPermission(user, theMethod, anot.get())) {
				abort(requestContext);
			}

		} catch (Exception e) {
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}
	}

	protected String getToken(ContainerRequestContext requestContext, UriInfo info)
			throws UnsupportedEncodingException {

		String token;
		String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		if (authorizationHeader != null) {
			// Check if the HTTP Authorization header is present and
			// formatted correctly
			if (authorizationHeader.startsWith("Bearer ")) {
				token = authorizationHeader.substring("Bearer".length()).trim();
			} else {
				token = authorizationHeader.trim();
			}
		} else {
			Cookie cook = requestContext.getCookies().get(HttpHeaders.AUTHORIZATION);
			if (cook != null) {
				token = URLDecoder.decode(cook.getValue(), "UTF-8");
			} else {
				token = info.getQueryParameters().getFirst("api_key");
				if (token == null) {
					token = httpRequest.getRemoteAddr();
				}
			}
		}
		return token;
	}

	protected void abort(ContainerRequestContext requestContext) {

		requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
				.entity("{ \"notAllowed\" : true, \"message\" : \"No tiene permiso para acceder a esta sección\"}")
				.build());
	}

	private boolean userHasPermission(Usuario user, Method theMethod, Logged logged) {

		Rol[] roles = logged.value();
		if (roles == null || roles.length == 0) {
			System.err.println("Método sin roles requeridos encontrado" + theMethod.toString());
			return false;
		}

		return hasRol(user, roles);
	}

	private boolean hasRol(Usuario u, Rol[] roles) {

		return this.logic.hasAnyRole(u, roles);
	}

	private Optional<Logged> getAnnotation() throws NoSuchMethodException, SecurityException {

		Method resourceMethod = resourceInfo.getResourceMethod();
		Method classMethod = resourceInfo.getResourceClass().getMethod(resourceMethod.getName(),
				resourceMethod.getParameterTypes());
		Logged methodAnnot = classMethod.getAnnotation(Logged.class);
		if (methodAnnot != null) {
			return Optional.of(methodAnnot);
		}

		Class<?> resourceClass = resourceInfo.getResourceClass();
		Logged classAnnot = resourceClass.getAnnotation(Logged.class);
		return Optional.ofNullable(classAnnot);

	}

}
