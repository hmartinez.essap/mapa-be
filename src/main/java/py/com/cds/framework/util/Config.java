package py.com.cds.framework.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.CDI;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;

import lombok.extern.log4j.Log4j2;

/**
 * Bean que facilita la confiravés de un archivo en el Sistema.
 *
 * @author Arturo Volpe
 *
 */
@ApplicationScoped
@Log4j2
public class Config {

	/**
	 * Limita la cantidad de opciones que tiene el autocomplete.
	 */
	private static final Integer LIMIT_AUTO_COMPLETE = 5;
	private static final String USE_ENVIRONMENT_FLAG = "USE_ENV";
	private static final String ENV_DEFAULT_PREFIX = "mawio";

	private static final String PROPERTY_FILE_LOCATION = "/";
	private static final String PROPERTY_FILE_NAME = "config.properties";

	private Properties properties;

	@PostConstruct
	public void init() {

		if (useEnvironment()) {
			log.info("Using environment variables");
			loadEnvironmentProperties();
		} else {
			log.info("Using file properties");
			loadFileProperties(PROPERTY_FILE_LOCATION);
		}

	}

	private void loadFileProperties(String filePath) {
		properties = new Properties();
		String configurationAbsolutePath = PROPERTY_FILE_LOCATION + System.getProperty("file.separator") + filePath;
		try (InputStream is = new FileInputStream(configurationAbsolutePath)) {
			properties.load(is);
		} catch (IOException fnfe) {
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);

			System.out.println("Se busca en resources del war");
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			try (InputStream stream = classloader.getResourceAsStream(PROPERTY_FILE_NAME)) {
				properties.load(stream);
			} catch (IOException e) {
				System.out.println(
						"No se puede obtener archivo de configuración desde resources, utilizando valores por defecto");
			}
		}
	}

	private static Config INSTANCE;

	public static Config getStatic() {

		if (INSTANCE == null) {
			INSTANCE = CDI.current().select(Config.class).get();
		}
		return INSTANCE;
	}

	public Integer getLimiteAutoComplete() {

		return getInteger("rest.result.limit", LIMIT_AUTO_COMPLETE);
	}

	public boolean isDevel() {

		return getBoolean("debug.enabled", false);
	}

	public String getString(String key) {

		String toRet = properties.getProperty(Validate.notNull(key, "Can't search with null key"));
		return Validate.notNull(toRet, "Can't find the key %s", key).trim();
	}

	/**
	 * Returns a integer property
	 *
	 * @param key
	 *            key of the property, can't be <code>null</code>
	 * @param def
	 *            default value, can be <code>null</code>
	 * @return if the value is found, the parsed value, if not, the default
	 *         value.
	 */
	public Integer getInteger(String key, Integer def) {

		String current = properties.getProperty(Validate.notNull(key, "Can't search with null key"));
		if (current == null) {
			return def;
		} else {
			return Integer.parseInt(current.trim());
		}

	}

	/**
	 * Returns a boolean property
	 *
	 * @param key
	 *            key of the property, can't be <code>null</code>
	 * @param def
	 *            default value, can be <code>null</code>
	 * @return if the value is found, the parsed value, if not, the default
	 *         value.
	 */
	public Boolean getBoolean(String key, Boolean def) {

		String current = properties.getProperty(Validate.notNull(key, "Can't search with null key"));
		if (current == null) {
			return def;
		} else {
			return Boolean.parseBoolean(current.trim());
		}

	}

	public String getString(String key, String de) {

		String toRet = properties.getProperty(Validate.notNull(key, "Can't search with null key"));
		return toRet == null ? de : toRet;
	}

	public void set(String string, String value) {

		this.properties.put(string, value);

	}

	public int getSessionTimeout() {

		return getInteger("session.timeout", 50000);
	}

	public int getSessionTtl() {

		return getInteger("session.ttl", 60);
	}

	public int getSessionMaxRequest() {

		return getInteger("session.limit", 100);
	}

	/**
	 * Builds or get a instance without the CDI container;
	 *
	 * @return
	 */
	public static Config getStaticWithoutCDI() {

		if (INSTANCE == null) {
			Config c = new Config();
			c.init();
			INSTANCE = c;
		}
		return INSTANCE;
	}

	/**
	 * If a system property called USE_ENVIRONMENT_FLAG exists, with value
	 * <code>true</code>, then we use the environment variables as keys.
	 */
	private boolean useEnvironment() {

		return Boolean.valueOf(System.getenv(USE_ENVIRONMENT_FLAG));
	}

	/**
	 * Parse all the environment variables that starts with MYTAPP (ignoring
	 * case), and put them in the properties.
	 * 
	 * Also the underscore(<code>_</code>) is replaced by a dot, translating:
	 * 
	 * <pre>
	 * "myTapp_forte_baseUrl" to "forte.baseUrl"
	 * </pre>
	 * 
	 */
	private void loadEnvironmentProperties() {

		Map<String, String> props = System.getenv();
		properties = new Properties();

		props.entrySet()

				.stream()

				.filter(e -> e.getKey().toLowerCase().startsWith(ENV_DEFAULT_PREFIX))

				.map(e -> ImmutablePair.of(mapEnvironmentName(e.getKey()), e.getValue()))

				.forEach(ip -> properties.put(ip.getKey(), ip.getValue()));

	}

	private String mapEnvironmentName(String sourceName) {
		String withoutPrefix = sourceName.substring(ENV_DEFAULT_PREFIX.length() + 1);

		return withoutPrefix.replaceAll("_", ".");
	}
}
