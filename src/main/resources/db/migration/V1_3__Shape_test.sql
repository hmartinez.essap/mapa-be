CREATE SEQUENCE shape_test_id_sequence;
CREATE TABLE shape_test (

	id bigint NOT NULL,

	shape GEOMETRY,
	
	CONSTRAINT shape_test_pkey PRIMARY KEY (id)

);
