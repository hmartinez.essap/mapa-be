ALTER TABLE shape ADD COLUMN estado_id bigint NOT NULL;

ALTER TABLE estado_shape ADD COLUMN codigo_linea  text;

INSERT INTO estado_shape(id,icono,tipo_linea,descripcion,codigo_linea) values(nextval('estado_shape_id_sequence'),'fa-line-chart','Punteada','En proyeccion','0.9, 10');
INSERT INTO estado_shape(id,icono,tipo_linea,descripcion,codigo_linea) values(nextval('estado_shape_id_sequence'),'fa-question-circle','Discontinua','Licitado','15, 10');
INSERT INTO estado_shape(id,icono,tipo_linea,descripcion,codigo_linea) values(nextval('estado_shape_id_sequence'),'fa-handshake-o','Discontinua-Punteada','Contratado','0.9, 10, 15, 10, 0.9, 10');
INSERT INTO estado_shape(id,icono,tipo_linea,descripcion,codigo_linea) values(nextval('estado_shape_id_sequence'),'fa-check-circle','Continua','Finalizado','Continua');
INSERT INTO estado_shape(id,icono,tipo_linea,descripcion,codigo_linea) values(nextval('estado_shape_id_sequence'),'fa-play','Discontua Larga','En ejecucion','2em');

UPDATE shape SET estado_id = 1;

ALTER TABLE shape ADD CONSTRAINT estado_shape_fkey FOREIGN KEY (estado_id) REFERENCES estado_shape (id) MATCH SIMPLE;
