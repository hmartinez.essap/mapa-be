ALTER TABLE contrato_dncp ALTER COLUMN ruc DROP NOT NULL;
ALTER TABLE contrato_dncp ALTER COLUMN nro_contrato DROP NOT NULL;
ALTER TABLE contrato_dncp ALTER COLUMN estado DROP NOT NULL;
ALTER TABLE contrato_dncp ALTER COLUMN fecha_contrato DROP NOT NULL;
ALTER TABLE contrato_dncp ALTER COLUMN proveedor_id DROP NOT NULL;