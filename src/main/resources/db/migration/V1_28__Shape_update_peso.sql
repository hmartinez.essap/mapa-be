UPDATE shape SET peso = ST_Area(shape.geometry) WHERE shape.tipo_geom = 'A_AREA';
UPDATE shape SET peso = ST_Length(shape.geometry) WHERE shape.tipo_geom = 'B_Longitud';
UPDATE shape SET peso = 99999999 WHERE shape.tipo_geom = 'C_Point';