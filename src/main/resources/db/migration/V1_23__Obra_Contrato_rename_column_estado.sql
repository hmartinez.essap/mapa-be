ALTER TABLE obra RENAME COLUMN estado TO visible;

UPDATE obra SET visible = 'SI' WHERE visible = 'ACTIVO';
UPDATE obra SET visible = 'NO' WHERE visible = 'INACTIVO';

ALTER TABLE contrato RENAME COLUMN estado TO visible;

UPDATE contrato SET visible = 'SI' WHERE visible = 'ACTIVO';
UPDATE contrato SET visible = 'NO' WHERE visible = 'INACTIVO';