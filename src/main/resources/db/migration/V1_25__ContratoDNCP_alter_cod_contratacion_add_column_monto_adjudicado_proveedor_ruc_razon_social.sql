ALTER TABLE contrato_dncp RENAME COLUMN cod_contratacion TO nro_contratacion;

ALTER TABLE contrato_dncp DROP COLUMN proveedor_id;
ALTER TABLE contrato_dncp DROP COLUMN nro_contrato;

ALTER TABLE contrato_dncp ADD COLUMN cod_contratacion text;
ALTER TABLE contrato_dncp ADD COLUMN monto_adjudicado bigint;
ALTER TABLE contrato_dncp ADD COLUMN moneda text;
ALTER TABLE contrato_dncp ADD COLUMN razon_social text;
ALTER TABLE contrato_dncp ADD COLUMN id_llamado text;
ALTER TABLE contrato_dncp ADD COLUMN link_dncp text;
