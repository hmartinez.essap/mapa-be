CREATE SEQUENCE estado_shape_id_sequence;

CREATE TABLE estado_shape
(
  id bigint NOT NULL,
  icono text NOT NULL,
  tipo_linea text NOT NULL,
  descripcion text NOT NULL,
  
CONSTRAINT estado_pkey PRIMARY KEY (id)
);