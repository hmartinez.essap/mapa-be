ALTER TABLE contrato ADD COLUMN cronograma_obra text NOT NULL DEFAULT 'SIN DEFINIR';
ALTER TABLE contrato ADD COLUMN fecha_firma_contrato date;
ALTER TABLE contrato ADD COLUMN monto_total bigint;
ALTER TABLE contrato ADD COLUMN monto_desembolsado bigint;
ALTER TABLE contrato ADD COLUMN convenio_adenda text;