CREATE SEQUENCE obra_id_sequence;

CREATE TABLE obra
(
  id bigint NOT NULL,
  area text NOT NULL,
  fecha_inicio date NOT NULL,
  fecha_fin date NOT NULL,
  tipo text NOT NULL,
  descripcion text NOT NULL,

  CONSTRAINT obra_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE proveedor_id_sequence;

CREATE TABLE proveedor
(
  id bigint NOT NULL,
  razon_social text NOT NULL,
  ruc text NOT NULL,
  pac_dncp text NOT NULL,
  link_dncp text NOT NULL,
  fecha_obtencion date NOT NULL,

  CONSTRAINT proveedor_pkey PRIMARY KEY (id)
);


CREATE SEQUENCE contrato_dncp_id_sequence;

CREATE TABLE contrato_dncp
(
  id bigint NOT NULL,
  ruc text NOT NULL,
  cod_contratacion bigint NOT NULL,
  nro_contrato bigint NOT NULL,
  estado text NOT NULL,
  fecha_contrato date NOT NULL,
  proveedor_id bigint NOT NULL,

  CONSTRAINT contrato_dncp_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE contrato_id_sequence;

CREATE TABLE contrato
(
  id bigint NOT NULL,
  fecha_inicio date NOT NULL,
  fecha_fin date NOT NULL,
  obra_id bigint NOT NULL,
  proveedor_id bigint NOT NULL,
  shape_id bigint NOT NULL,
  link_dncp text NOT NULL,
  estado text NOT NULL,
  contrato_dncp_id bigint NULL,

  CONSTRAINT contrato_pkey PRIMARY KEY (id),
  CONSTRAINT proveedor_fkey FOREIGN KEY (proveedor_id) REFERENCES proveedor (id) MATCH SIMPLE,
  CONSTRAINT obra_fkey FOREIGN KEY (obra_id) REFERENCES obra (id) MATCH SIMPLE,
  CONSTRAINT contrato_dncp_fkey FOREIGN KEY (contrato_dncp_id) REFERENCES contrato_dncp (id) MATCH SIMPLE
);

CREATE SEQUENCE tipo_shape_id_sequence;

CREATE TABLE tipo_shape
(
  id bigint NOT NULL,
  icono text NOT NULL,
  color text NOT NULL,
  descripcion text NOT NULL,

  CONSTRAINT tipo_pkey PRIMARY KEY (id)
);


CREATE SEQUENCE shape_id_sequence;

CREATE TABLE shape
(
  id bigint NOT NULL,
  geometry geometry,
  contrato_id bigint,
  obra_id bigint NOT NULL,
  tipo_id bigint NOT NULL,
  descripcion text NOT NULL,

  CONSTRAINT shape_pkey PRIMARY KEY (id),
  
  CONSTRAINT contrato_fkey FOREIGN KEY (contrato_id) REFERENCES contrato (id) MATCH SIMPLE,
  CONSTRAINT obra_fkey FOREIGN KEY (obra_id) REFERENCES obra (id) MATCH SIMPLE,
  CONSTRAINT tipo_fkey FOREIGN KEY (tipo_id) REFERENCES tipo_shape (id) MATCH SIMPLE
);


CREATE SEQUENCE foto_id_sequence;

CREATE TABLE foto
(
  id bigint NOT NULL,
  shape_id bigint NOT NULL,
  hash text NOT NULL,
  
  CONSTRAINT foto_pkey PRIMARY KEY (id),
  CONSTRAINT shape_fkey FOREIGN KEY (shape_id) REFERENCES shape (id) MATCH SIMPLE
);

CREATE SEQUENCE comentario_id_sequence;

CREATE TABLE comentario
(
  id bigint NOT NULL,
  comentario text NOT NULL,
  posicion point NOT NULL,
  email text NOT NULL,
  fecha_emision date NOT NULL,
  fecha_respuesta date NOT NULL,
  respuesta text NOT NULL,
  estado text NOT NULL,
  usuario_respuesta bigint NOT NULL,
  shape_id bigint NOT NULL,
  
  CONSTRAINT comentario_pkey PRIMARY KEY (id),
  CONSTRAINT shape_fkey FOREIGN KEY (shape_id) REFERENCES shape (id) MATCH SIMPLE,
  CONSTRAINT usuario_fkey FOREIGN KEY (usuario_respuesta) REFERENCES usuario (id) MATCH SIMPLE
);

CREATE SEQUENCE rol_id_sequence;
CREATE TABLE rol
(
  id bigint NOT NULL,
  descripcion text NOT NULL,
  
  CONSTRAINT roles_pkey PRIMARY KEY (id)
);


CREATE TABLE usuario_rol
(
  user_id bigint NOT NULL,
  rol_id bigint NOT NULL,
  
  CONSTRAINT usuario_rol_pkey PRIMARY KEY (user_id, rol_id),
  
  CONSTRAINT rol_fkey FOREIGN KEY (rol_id) REFERENCES rol (id) MATCH SIMPLE,
  CONSTRAINT usuario_fkey FOREIGN KEY (user_id) REFERENCES usuario (id) MATCH SIMPLE
);

