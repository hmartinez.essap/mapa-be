ALTER TABLE comentario DROP COLUMN posicion;
ALTER TABLE comentario ADD COLUMN latitud double precision NOT NULL;
ALTER TABLE comentario ADD COLUMN longitud double precision NOT NULL;