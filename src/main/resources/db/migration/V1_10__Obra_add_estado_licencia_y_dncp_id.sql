ALTER TABLE obra ADD COLUMN dncp_id bigint NOT NULL default -1;
ALTER TABLE obra ADD COLUMN estado text NOT NULL default 'ACTIVO';
ALTER TABLE obra ADD COLUMN licencia_ambiental text NOT NULL default 'SIN DEFINIR';
