ALTER TABLE shape ADD COLUMN tipo_geom text;
ALTER TABLE shape ADD COLUMN peso double precision;

UPDATE shape SET tipo_geom = 'A_AREA' WHERE ST_GeometryType(shape.geometry) = 'ST_Polygon';
UPDATE shape SET tipo_geom = 'B_Longitud' WHERE ST_GeometryType(shape.geometry) = 'ST_LineString';
UPDATE shape SET tipo_geom = 'C_Punto' WHERE ST_GeometryType(shape.geometry) = 'ST_Point';