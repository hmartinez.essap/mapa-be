ALTER TABLE audit.logged_actions ADD COLUMN application_user text DEFAULT '';
ALTER TABLE audit.logged_actions ADD COLUMN application_ip text DEFAULT '';