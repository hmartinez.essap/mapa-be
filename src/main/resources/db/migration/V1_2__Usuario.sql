CREATE SEQUENCE usuario_id_sequence;
CREATE TABLE usuario (

	id bigint NOT NULL,

	username text NOT NULL,
	nombre text NOT NULL,

	dependencia text NOT NULL,
	
	correo text NOT NULL,
	contrasenha text NOT NULL,
	estado text NOT NULL,
	
	
	CONSTRAINT usuario_pkey PRIMARY KEY (id)

);

INSERT INTO usuario VALUES (-1000, 'admin', 'admin', 'ESSAP', 'admin@admin.com', '0403e48c9ddde0d5b3d6fc3cb60c724d', 'ACTIVO');