#/bin/bash

set -e

if [ -f $JBOSS_HOME/.jboss_admin_pass_configured ]; then
    echo "JBoss admin user's password has been configured!"
    exit 0
fi

#generate password
PASS=${JBOSS_PASS:-$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 25 ; echo '')}
_word=$( [ ${JBOSS_PASS} ] && echo "preset" || echo "random" )

echo "=> Configuring admin user with a ${_word} password in Wildfly"
sh $JBOSS_HOME/bin/add-user.sh --silent=true admin ${PASS}
echo "=> Done!"
echo "========================================================================"
echo "You can now configure to this JBoss server using:"
echo ""
echo "    admin:${PASS}"
echo ""
echo "========================================================================"

touch $JBOSS_HOME/.jboss_admin_pass_configured
