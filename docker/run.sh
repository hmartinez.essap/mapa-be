#/bin/bash

set -e

if [ "$JBOSS_PASS" = "**Random**" ]; then
    unset JBOSS_PASS
fi

if [ ! -f $JBOSS_HOME/.jboss_admin_pass_configured ]; then
    /configure.sh
fi

echo "========================================================================"
echo "                          Running wildfly"
echo "========================================================================"
$JBOSS_HOME/bin/standalone.sh -bmanagement 0.0.0.0 -b 0.0.0.0